<?php
 
 require('connect.php');
 
    $id = $conn -> real_escape_string($_POST['id']);
   
	$qry = Qry($conn, "SELECT claim_branch,claim_ho,frno,veh_type FROM rrpl_database.rcv_pod WHERE id = '$id'");
	
	if(!$qry){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		exit();
	}
	
	if(numRows($qry)==0)
	{
		echo "<script> alert('ERROR: POD not found..'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		exit();
	}
	
	$row = fetchArray($qry);
	
if($row['claim_branch']=="1")
{
	$chk_entry = Qry($conn, "SELECT id FROM rrpl_database.claim_book_trans WHERE pod_id = '$id' AND main_entry!='1'");
	
	if(!$chk_entry){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		exit();
	}
	
	if(numRows($chk_entry)>0)
	{
		echo "<script> alert('ERROR: Claim amount deducted already.'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		exit();
	}
}
	
/** update only if balance pending ***/

	// $qry2 = Qry($conn, "SELECT paidto FROM rrpl_database.freight_form WHERE frno = '$row[frno]'");
	
	// if(!$qry2){ 
		// $error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		// echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		// exit();
	// }
	
	// if(numRows($qry2)==0 AND $row['veh_type']=='MARKET')
	// {
		// echo "<script> alert('ERROR: Freight memo not found..'); $('#loadicon').hide();</script>";  
		// exit();
	// }
	
	// $row2 = fetchArray($qry2);
	
	// if($row2['paidto']!='' AND $row['veh_type']=='MARKET')
	// {
		// echo "<script>
			// Swal.fire({
				// position: 'top-end',
				// icon: 'error',
				// title: 'Balance paid',
				// showConfirmButton: false,
				// timer: 1000
			// })
			// $('#ClaimSel$id').val('');
			// $('#loadicon').hide();
		// </script>";
		// exit();
	// }

/** update only if balance pending ***/

StartCommit($conn);
$flag = true;	

	if($row['claim_ho']=="1")
	{
		$dlt_old_records = Qry($conn,"DELETE FROM claim_records_admin WHERE pod_id='$id'");
		
		if(!$dlt_old_records){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$dlt_old_records2 = Qry($conn,"DELETE FROM claim_records_admin_desc WHERE pod_id='$id'");
		
		if(!$dlt_old_records2){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($row['claim_branch']=="1")
	{
		$dlt_old_records1 = Qry($conn,"DELETE FROM claim_records WHERE pod_id='$id'");
		
		if(!$dlt_old_records1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$dlt_old_records22 = Qry($conn,"DELETE FROM claim_records_desc WHERE pod_id='$id'");
		
		if(!$dlt_old_records22){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$dlt_txns = Qry($conn,"DELETE FROM claim_book_trans WHERE pod_id='$id'");
		
		if(!$dlt_txns){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update1 = Qry($conn, "UPDATE rrpl_database.rcv_pod SET claim_branch='2',claim_ho='2' WHERE id = '$id'");
	
		if(!$update1){ 
			$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
			echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
			exit();
		}
	}
	else
	{
		$update1 = Qry($conn, "UPDATE rrpl_database.rcv_pod SET claim_ho='2' WHERE id = '$id'");
	
		if(!$update1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	} 

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			Swal.fire({
				position: 'top-end',
				icon: 'success',title: 'Status updated',
				showConfirmButton: false,
				timer: 1000
			})
			$('#loadicon').hide();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script>
			Swal.fire({
				position: 'top-end',
				icon: 'error',title: 'Error !!',
				showConfirmButton: false,
				timer: 1000
			})
			$('#ClaimSel$id').val('');
			$('#loadicon').hide();
		</script>";
}

	// $status="icon: 'success',title: 'POD Approved',";
   // }else if($stat=="-1"){
   	// $status="icon: 'error',title: 'POD Rejected',";
	
?> 