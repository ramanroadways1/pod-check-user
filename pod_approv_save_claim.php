<?php
require('connect.php'); 
$timestamp = date("Y-m-d H:i:s");
 
$pod_id = $conn->real_escape_string($_POST['pod_id']);
$lrno = $conn->real_escape_string($_POST['lrno']);
$vou_no = $conn->real_escape_string($_POST['vou_no']);

$chk_record=Qry($conn,"SELECT id FROM rrpl_database.claim_records_admin WHERE pod_id='$pod_id'");
if(!$chk_record){
	echo "<script>
		alert('Error !!');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

if(numRows($chk_record)>0)
{
	echo "<script>
		alert('Duplicate record found.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

$date=date("Y-m-d");

if(empty($_POST['claim_amount']))
{
	echo "<script type='text/javascript'>
		alert('No record added.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

$total_count=count($_POST['claim_amount']);

if($total_count<=0)
{
	echo "<script type='text/javascript'>
		alert('No record added.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}
	
if($_POST['total_claim_amount_db']!=array_sum($_POST['claim_amount']))
{
	echo "<script type='text/javascript'>
		alert('Claim amount not verified.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

$damage_amount = 0;
$shortage_amount = 0;

for($i=0; $i<$total_count;$i++) 
{
	if($_POST['claim_type'][$i]=="Damage")
	{
		$damage_amount = $_POST['claim_amount'][$i];
	}
	
	if($_POST['claim_type'][$i]=="Shortage")
	{
		$shortage_amount = $_POST['claim_amount'][$i];
	}
}

if(($damage_amount+$shortage_amount)!=$_POST['total_claim_amount_db'])
{
	echo "<script type='text/javascript'>
		alert('Total amount and damage+shortage amount not verified.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;	

$insert_main_record = Qry($conn,"INSERT INTO rrpl_database.claim_records_admin(lrno,vou_no,pod_id,amount,damage_amount,shortage_amount,
balance_amount,branch,timestamp) VALUES ('$lrno','$vou_no','$pod_id','$_POST[total_claim_amount_db]','$damage_amount','$shortage_amount',
'$_POST[total_claim_amount_db]','HO','$timestamp')");

if(!$insert_main_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$claim_id = getInsertID($conn);

for($i=0; $i<$total_count;$i++) 
{
    $insert_claim = Qry($conn,"INSERT INTO rrpl_database.claim_records_admin_desc(claim_id,pod_id,amount,claim_type,
	claim_unit,unit_value,narration,timestamp) VALUES ('".$claim_id."','$pod_id','".$_POST['claim_amount'][$i]."',
	'".$_POST['claim_type'][$i]."','".$_POST['claim_unit'][$i]."','".$_POST['unit_value'][$i]."','".$_POST['narration'][$i]."',
	'".$timestamp."')");
	
	if(!$insert_claim){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_claim_status = Qry($conn,"UPDATE rrpl_database.rcv_pod SET claim_ho='1' WHERE id='$pod_id'");

if(!$update_claim_status){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

$dlt_cache = Qry($conn,"DELETE FROM rrpl_database.claim_cache_admin");

if(!$dlt_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$chk_txn = Qry($conn,"SELECT id,balance,lrno,vou_no FROM claim_book_trans WHERE pod_id='$pod_id' AND main_entry='1'");

if(!$chk_txn){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_txn)>0)
{
	$row_txn = fetchArray($chk_txn);
	$branch_bal = $row_txn['balance'];
	
	$diff_amt = $_POST['total_claim_amount_db']-$branch_bal;
	
	$update_amount = Qry($conn,"UPDATE claim_book_trans SET credit=credit-('$diff_amt'),balance=balance-('$diff_amt'),
	branch_amount='$branch_bal',admin_amount='$_POST[total_claim_amount_db]' WHERE pod_id='$row_txn[id]'");

	if(!$update_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_amount_nxt = Qry($conn,"UPDATE claim_book_trans SET balance=balance-('$diff_amt') WHERE 
	id>'$row_txn[id]' AND lrno='$row_txn[lrno]' AND vou_no='$row_txn[vou_no]'");

	if(!$update_amount_nxt){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	if (strpos($vou_no, 'OLR') !== false){
		$vou_type='OWN';
	}
	else{
		$vou_type='MARKET';
	}

	$insert_amt = Qry($conn,"INSERT INTO claim_book_trans(pod_id,lrno,vehicle_type,vou_no,branch,date,credit,balance,
	admin_amount,narration,main_entry,timestamp) VALUES ('$pod_id','$lrno','$vou_type','$vou_no','HO','$date',
	'$_POST[total_claim_amount_db]','$_POST[total_claim_amount_db]','$_POST[total_claim_amount_db]','Claim on POD',
	'1','$timestamp')");

	if(!$insert_amt){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Record Added !');
			$('#ClaimSel$pod_id').attr('disabled',true);
			$('#ClaimSel$pod_id').hide();
			$('#claim_view_btn$pod_id').show();
			$('#loadicon').hide();
			$('#claimModal').modal('hide');  
			$('#btn_claim_submit').attr('disabled',false);
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

?>