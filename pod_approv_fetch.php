<?php

require('connect.php');

$fromdate = $_REQUEST['a'];
$todate = $_REQUEST['b'];
// $cardno = $_REQUEST['c'];
 
ini_set('memory_limit', '-1');

	$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_api.';', $DATABASE_USER, $DATABASE_PASS );
	
	$statement = $connection->prepare("SELECT p.claim_branch,f2.paidto,p.claim_ho,p.remark,p.veh_type,f.crossing,p.id,
	p.ho_pod_check,f.fstation,(f2.adv_claim+f2.claim) as claim_deducted,f.tstation,DATE_FORMAT(l.date,'%d/%m/%y') as lr_date,l.truck_no as lr_tno,l.dest_zone, p.frno,p.lrno,p.consignor_id,p.branch as pod_rcvr,DATE_FORMAT(fm_date,'%d/%m/%y') as fm_date,fm_amount,fm_adv,fm_bal,
	p.pod_branch as fm_branch,pod_copy,DATE_FORMAT(p.pod_date,'%d/%m/%y') as pod_date,DATE_FORMAT(del_date,'%d/%m/%y') as del_date,DATE_FORMAT(f2.bal_date,'%d/%m/%y') as bal_date,late_charge,billing_ofc,billing_time,f.truck_no,f.consignor, 
	f.consignee,f.wt12,f.weight,f.claim_entry,emps.name,claim.rcvd_amount as claim_rcv_amount,u.z as is_coal_branch,coal.claim as coal_claim 
	FROM rrpl_database.rcv_pod as p 
	left join rrpl_database.emp_attendance as emps on emps.code = p.branch_user
	LEFT JOIN rrpl_database.lr_sample as l on l.lrno=p.lrno 
    left join rrpl_database.freight_form_lr as f on f.frno = p.frno and f.lrno=p.lrno
    left join rrpl_database.freight_form as f2 on f2.frno=p.frno 
    left join rrpl_database.claim_records_admin as claim on claim.pod_id=p.id 
    left join rrpl_database.user as u on u.username=p.pod_branch 
    left join ship.freight_memo_bal as coal on coal.fm_no=p.frno 
    WHERE p.pod_date between '$fromdate' AND '$todate' and p.frno not like '___M%' 
	group by p.id ORDER BY p.id ASC");
    
    // $statement = $connection->prepare("SELECT rcv_pod.remark, rcv_pod.veh_type, lr_sample.crossing,rcv_pod.id,rcv_pod.ho_pod_check,lr_sample.fstation, lr_sample.tstation, lr_sample.dest_zone, rcv_pod.frno, rcv_pod.lrno, rcv_pod.branch as pod_rcvr,fm_date,fm_amount,fm_adv,fm_bal,pod_branch as fm_branch,pod_copy,pod_date, del_date,bal_date,late_charge,billing_ofc,billing_time, lr_sample.truck_no, lr_sample.consignor, lr_sample.consignee, lr_sample.wt12, lr_sample.weight, emps.name FROM rrpl_database.rcv_pod as rcv_pod 

    //     left join rrpl_database.emp_attendance as emps on emps.code = rcv_pod.branch_user
    //     left join rrpl_database.lr_sample as lr_sample on lr_sample.lrno=rcv_pod.lrno 
    //     WHERE (pod_date between '$fromdate' AND '$todate') and consignor_id!='56' and rcv_pod.frno not like '___M%'
    //     group by rcv_pod.id
    //     ORDER BY rcv_pod.id ASC");
	
$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

foreach($result as $row)
{ 
    $sub_array = array(); 
 
    $a = $b = "";
    if ($row["ho_pod_check"] == "1" ){
    $a = 'selected' ;
    }
    else if ($row["ho_pod_check"] == "-1" ){
    $b = 'selected' ;
    } 

if($row['consignor_id']=='56')
{
	$sub_array[] = "Finetech";
}
else
{
    $sub_array[] = "<center><select id='ApprovalSelectBox$row[id]' onchange='update(".$row['id'].", this.value,".$row['claim_ho'].")'   style='width: 100px; min-height:22px;'> 
    <option value='0'> -- select --  </option>
    <option value='1' ".$a."> Approv </option>
    <option value='-1' ".$b."> Reject  </option> 
    </select></center>";
}

if($row['consignor_id']=='56')
{
	$sub_array[] = "Finetech";
}
else
{
	if ($row["ho_pod_check"] == "1" ){
	$sub_array[] = "Approved";    
	}
	else if ($row["ho_pod_check"] == "-1" ){
	$sub_array[] = "Rejected";    
	} else {
	$sub_array[] = "Pending";    
	}
}

if($row['veh_type']=='MARKET')
{
	if ($row["claim_branch"] == "1" ){
	$sub_array[] = "<font color='red'>YES</font>";    
	}
	else if ($row["claim_branch"] == "2" ){
	$sub_array[] = "<font color='green'>NO</font>";    
	} else {
	$sub_array[] = "<font color='red'>--</font>";    
	}
	
	if($row['is_coal_branch']=="1"){
		$claim_deducted = $row['coal_claim'];
	}
	else{
		$claim_deducted = $row['claim_deducted'];
	}
}
else
{
	if ($row["claim_entry"] == "1" ){
	$sub_array[] = "<font color='red'>YES</font>";    
	}
	else if ($row["claim_entry"] == "2" ){
	$sub_array[] = "<font color='green'>NO</font>";    
	} else {
	$sub_array[] = "<font color='red'>--</font>";    
	}

	// $chk_claim_amount = Qry($conn,"SELECT COALESCE(SUM(debit),0) as claim_amount FROM rrpl_database.claim_book WHERE lrno='$row[lrno]' AND 
	// vou_no='$row[frno]'");
	
	// $row_claim_amt = fetchArray($chk_claim_amount);
	$claim_deducted = $row['claim_rcv_amount'];
}

	$a1 = $b1 = "";
    if ($row["claim_ho"] == "1" ){
    $a1 = 'selected' ;
	$is_claim="<a href='#' onclick='ViewClaimData($row[id])' style='color:blue;cursor:pointer'>Yes</a>";
    }
    else if ($row["claim_ho"] == "2" ){
    $b1 = 'selected' ;
	$is_claim="No";
    }
	
	if($row["claim_ho"]=="1" || $row["claim_ho"]=="2")
	{
		if($row["claim_ho"]=="1")
		{
			$sub_array[] = "<center><select id='ClaimSel$row[id]' onchange=updateClaim('$claim_deducted','$row[id]',this.value) style='width: 100px; min-height:22px;'> 
			<option value=''> -- select --  </option>
			<option value='1' ".$a1."> Yes </option>
			<option value='2' ".$b1."> No Claim  </option> 
			</select> </center> <a href='#' id='claim_view_btn$row[id]' style='color:blue;cursor:pointer' onclick='ViewClaimData($row[id])'>Yes</a>";
		}
		else
		{
			$sub_array[] = "<center><select id='ClaimSel$row[id]' onchange=updateClaim('$claim_deducted','$row[id]',this.value) style='width: 100px; min-height:22px;'> 
				<option value=''> -- select --  </option>
				<option value='1' ".$a1."> Yes </option>
				<option value='2' ".$b1."> No Claim  </option> 
			</select> </center> <a href='#' id='claim_view_btn$row[id]' style='display:none;color:blue;cursor:pointer' onclick='ViewClaimData($row[id])'>Yes</a>";
		}
	}
	else
	{
		$sub_array[] = "<center><select id='ClaimSel$row[id]' onchange=updateClaim('$claim_deducted','$row[id]',this.value) style='width: 100px; min-height:22px;'> 
		<option value=''> -- select --  </option>
		<option value='1' ".$a1."> Yes </option>
		<option value='2' ".$b1."> No Claim  </option> 
		</select> </center> <a href='#' id='claim_view_btn$row[id]' style='color:blue;display:none;cursor:pointer' onclick='ViewClaimData($row[id])'>Yes</a>";
	}

if($row['is_coal_branch']=="1")
{
	if($row['coal_claim']=="")
	{
		$bal_status="Not paid";
	}
	else
	{
		$bal_status="<font color='red'>Paid</font>";
	}
}
else
{
	if($row['paidto']=='')
	{
		$bal_status="Not paid";
	}
	else
	{
		$bal_status="<font color='red'>Paid</font>";
	}
}

if($row['veh_type']!='MARKET')
{
	$bal_status="";
	$fm_no = "$row[frno]";
}
else
{
	if($row['is_coal_branch']=="1")
	{
		$fm_no = "<a style='color:blue;cursor: pointer' onclick=$('#fm_no_search_coal_id').val('$row[frno]');document.getElementById('form_fm_search_coal').submit()>$row[frno]</a>";
	}
	else
	{
		$fm_no = "<a style='color:blue;cursor: pointer' onclick=$('#fm_no_search').val('$row[frno]');document.getElementById('form_fm_search').submit()>$row[frno]</a>";
	}
}

$pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['pod_copy']) as $pod_copies)
  {
    $copy_no++;
          
          if (strpos($pod_copies, 'pdf') !== false) {
          $file = 'PDF';
          } else {
          $file = 'IMAGE';
          }

      if($row['veh_type']=="MARKET"){
        $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
      } else {
        $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
      }
   }

    $sub_array[] = $bal_status; 
    $sub_array[] = $claim_deducted; 
	$sub_array[] = $row["lr_date"]; 
	$sub_array[] = $row["del_date"]; 
	
    $sub_array[] = $row["fm_date"]; 

    $sub_array[] = $fm_no; 
    $sub_array[] = $row["lrno"]; 
	if($row["crossing"]=='YES'){
		$is_crossing="<font color='red'>Yes</font>";
	}
	else{
		$is_crossing="NO";
	}
	
	if($row["lr_tno"]!=$row["truck_no"]){
		$is_crossed="<font color='red'>Yes</font>";
	}
	else{
		$is_crossed="NO";
	}
	
	$sub_array[] = $is_crossing; 
	$sub_array[] = $is_crossed; 
    $sub_array[] = $row["truck_no"]; 
    $sub_array[] = round($row["wt12"],2); 
    $sub_array[] = round($row["weight"],2); 
    $sub_array[] = $row["fstation"]; 
    $sub_array[] = $row['tstation'];
	if($row['pod_copy']!=''){
$sub_array[] = implode(", ",$pod_files1);
} else {
$sub_array[] = "Not Found";
}
    $sub_array[] = $row['dest_zone'];
    $sub_array[] = $row["pod_rcvr"]; 
    $sub_array[] = htmlspecialchars($row["name"]); 
  // $sub_array[] = "";//$row["pod_copy"]; 
    $sub_array[] = $row["pod_date"]; 
    $sub_array[] = htmlspecialchars($row["consignor"]); 
    $sub_array[] = htmlspecialchars($row["consignee"]); 
    $sub_array[] = $row["fm_branch"]; 
    $sub_array[] = $row["fm_amount"]; 
    $sub_array[] = $row["fm_adv"]; 
    $sub_array[] = $row["fm_bal"]; 
    $sub_array[] = $row["bal_date"]; 
    $sub_array[] = $row["late_charge"]; 
    $sub_array[] = htmlspecialchars($row["billing_ofc"]); 
    $sub_array[] = $row["billing_time"]; 
    $sub_array[] = htmlspecialchars($row["remark"]); 
 
    $data[] = $sub_array;

} 

    $results = array(
	  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit();
?>
 