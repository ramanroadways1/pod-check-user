<?php

  require('connect.php');
   
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
      $statement = $connection->prepare("SELECT * FROM lr_remark");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
  $sub_array[] = "<center> <button onclick='update(".$row['id'].")' class='btn btn-sm btn-warning'> <i class='fa fa-edit '></i> <b> UPDATE  </b> </button> </center>"; 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['id']); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['title']); 
  $data[] = $sub_array;
} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>