<?php

	require('connect.php');

	$id = $conn -> real_escape_string($_POST['id']);
   	$status="icon: 'success',title: 'Mail Send',";

	$sqlt = "select * from rrpl_database.lr_mail where id='$id'";
	$rest = $conn->query($sqlt);
	$rowt = $rest->fetch_assoc();

	$sqlm = "SELECT * FROM rrpl_database.user where role='2' and username='$rowt[branch]'";
	$resm = $conn->query($sqlm);
	$rowm = $resm->fetch_assoc();
	$getemail = $rowm['email'];

	require 'PHPMailerAutoload.php';

	$email = 'noreply@ramanroadways.com';                   
	$password = 'rrpl123#';

	if($mail_testing=="1"){
	$to_id = "rrpl.msk@gmail.com";
	} else {
	$to_id = $getemail;
	}

	 $message = '';

	 if($rowt['type']=="LR"){
 				

	 			$sqli = "SELECT lr_sample.crossing,rcv_pod.memono, rcv_pod.pod_copy_rear, rcv_pod.id, rcv_pod.remark,rcv_pod.ho_pod_check,lr_sample.fstation, lr_sample.tstation, lr_sample.dest_zone, frno, rcv_pod.lrno, rcv_pod.branch as pod_rcvr,fm_date,fm_amount,fm_adv,fm_bal,pod_branch as fm_branch,pod_copy, veh_type,pod_date, del_date,bal_date,late_charge,billing_ofc,billing_time FROM rrpl_database.rcv_pod as rcv_pod LEFT JOIN rrpl_database.lr_sample as lr_sample on lr_sample.lrno=rcv_pod.lrno WHERE rcv_pod.id in (select refid from rrpl_database.lr_mail where mail='0' and status='-1' and branch='$rowt[branch]' and batchid='$rowt[batchid]' and type='$rowt[type]')";
				$resi = $conn->query($sqli);

				$message .= ' 
				<br>
				<table  border="1" style="border-collapse: collapse;  border: 1px solid black;" cellpadding="10">
				<tr>
					<td style="font-weight:bold;"> POD STATUS </td>
					<td style="font-weight:bold;"> POD REMARKS </td>
					<td style="font-weight:bold;"> FM NO </td>
					<td style="font-weight:bold;"> LR NO </td>
					<td style="font-weight:bold;"> POD RECEIVER </td>
					<td style="font-weight:bold;"> FM DATE </td>
					<td style="font-weight:bold;"> FM AMOUNT </td>
					<td style="font-weight:bold;"> POD  COPY </td> 
					<td style="font-weight:bold;"> POD DATE </td>
					<td style="font-weight:bold;"> DELIVERY DATE </td>
					<td style="font-weight:bold;"> BALANCE DATE </td>
					<td style="font-weight:bold;"> LATE CHARGE </td>
					<td style="font-weight:bold;"> BILLING OFFICE </td> 
				</tr>

				';

				while($rowi = $resi->fetch_assoc()){

				$pod_files1 = array(); 
				$copy_no = 0;
				foreach(explode(",",$rowi['pod_copy']) as $pod_copies)
				  {
				    $copy_no++;
				          
				          if (strpos($pod_copies, 'pdf') !== false) {
				          $file = 'PDF';
				          } else {
				          $file = 'IMAGE';
				          }

				      if($rowi['veh_type']=="MARKET"){
				        $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
				      } else {
				        $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
				      }
				   } 

					if($rowi['pod_copy']!=''){
					$podcopy = implode(", ",$pod_files1);
					} else {
					$podcopy = "Not Found";
					} 

					$message .= '<tr>
					<td> <font color=red> REJECTED </font> </td>
					<td> '.$rowi['remark'].' </td>
					<td> '.$rowi['frno'].' </td>
					<td> '.$rowi['lrno'].' </td>
					<td> '.$rowi['pod_rcvr'].' </td>
					<td> '.$rowi['fm_date'].' </td>
					<td> '.$rowi['fm_amount'].' </td>
					<td> '.$podcopy.' </td> 
					<td> '.$rowi['pod_date'].' </td> 
					<td> '.$rowi['del_date'].' </td>
					<td> '.$rowi['bal_date'].' </td>
					<td> '.$rowi['late_charge'].' </td>
					<td> '.$rowi['billing_ofc'].' </td>
					</tr>';
				}

				$message .= '</table>';
				$subject = $rowt['branch'].' POD Rejected of '.$rowt['batchid'].' '; 

	 }

	 if($rowt['type']=="EXP"){
 				

	 			$sqli = "SELECT * from rrpl_database.mk_venf WHERE id in (select refid from rrpl_database.lr_mail where mail='0' and status='-1' and branch='$rowt[branch]' and batchid='$rowt[batchid]' and type='$rowt[type]')";
				$resi = $conn->query($sqli);

				$message .= ' 
				<br>
				<table  border="1" style="border-collapse: collapse;  border: 1px solid black;" cellpadding="10">
				<tr>
					<td style="font-weight:bold;"> STATUS </td>
					<td style="font-weight:bold;"> REMARKS </td>
					<td style="font-weight:bold;"> BRANCH </td>
					<td style="font-weight:bold;"> VOUCHER NO </td>
					<td style="font-weight:bold;"> VOCUHER DATE </td>
					<td style="font-weight:bold;"> SYSTEM DATE </td>
					<td style="font-weight:bold;"> COMPANY </td>
					<td style="font-weight:bold;"> PARTICULARS </td> 
					<td style="font-weight:bold;"> VEHICLE NO </td>
					<td style="font-weight:bold;"> AMOUNT </td>
					<td style="font-weight:bold;"> PAY MODE </td>
					<td style="font-weight:bold;"> NARRATION </td>
					<td style="font-weight:bold;"> UPLOAD </td> 
				</tr>

				';

				while($rowi = $resi->fetch_assoc()){

					$pod_files1 = array(); 
					$copy_no = 0;
					foreach(explode(",",$rowi['upload']) as $pod_copies)
					{
					$copy_no++;
					$pod_files1[] = "<a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>Upload: $copy_no</a>";
					}

					if($rowi['upload']!=''){
					$podcopy = implode(", ",$pod_files1);
					} else {
					$podcopy = "Not Found";
					} 

					$message .= '<tr>
					<td> <font color=red> REJECTED </font> </td>
					<td> '.$rowi['remark'].' </td>
					<td> '.$rowi['user'].' </td>
					<td> '.$rowi['vno'].' </td>
					<td> '.$rowi['newdate'].' </td>
					<td> '.$rowi['date'].' </td>
					<td> '.$rowi['comp'].' </td>
					<td> '.$rowi['des'].' </td> 
					<td> '.$rowi['vehno'].' </td>
					<td> '.$rowi['amt'].' </td>
					<td> '.$rowi['chq'].' </td>
					<td> '.$rowi['narrat'].' </td>
					<td> '.$podcopy.' </td> 
					</tr>';
				}

				$message .= '</table>';
				$subject = $rowt['branch'].' Expense Voucher Rejected of '.$rowt['batchid'].' '; 

	 }
				$message .= "";
				$mail = new PHPMailer;
				$mail->isSMTP();
				$mail->Host = 'smtp.rediffmailpro.com';
				$mail->Port = 587;
				$mail->SMTPSecure = 'tls';
				$mail->SMTPAuth = true;
				$mail->Username = $email;
				$mail->Password = $password;
				$mail->setFrom('noreply@ramanroadways.com', 'Raman Roadways');
				$mail->addReplyTo('data@ramanroadways.com', 'Raman Roadways');
				$mail->addAddress($to_id);
				$mail->AddCC('data@ramanroadways.com', 'Raman Roadways');
				$mail->Subject = $subject;
				$mail->msgHTML($message);
 
				if(!$mail->send())
				{
				$error = "MAIL NOT SEND: " . $mail->ErrorInfo;
				echo "<script> alert('$error'); </script>";
				exit();
				}  else { 

							$qry = mysqli_query($conn, "update rrpl_database.lr_mail set mail='1' where status='-1' and branch='$rowt[branch]' and batchid='$rowt[batchid]' and type='$rowt[type]'");
							if(!$qry)
							{
								$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
								echo "<script> alert('ERROR: $error'); </script>";  

							} else {
								echo "
								<script>
								Swal.fire({
								position: 'top-end',
								".$status."
								showConfirmButton: false,
								timer: 1000
								})
								</script>";
							} 
				} 