<?php 
require('connect.php');

function before ($thiss, $inthat)
{
    return substr($inthat, 0, strpos($inthat, $thiss));
}

function after ($thiss, $inthat)
{
    if (!is_bool(strpos($inthat, $thiss)))
        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
}


$daterange = $_POST['daterange'];


$fromdate = before('-', $daterange); $fromdate = date("Y-m-d", strtotime($fromdate)); 
$todate = after('-', $daterange); $todate = date("Y-m-d", strtotime($todate));
 
 
$date1=date_create($fromdate);
$date2=date_create($todate);
$diff = date_diff($date1,$date2);
$diff = $diff->format("%a");
if($diff>31){
  echo "<script> alert('Date difference can\'t be more than 31 days !'); </script>";
  exit();
}

?>
<div class="card-body" style="padding-top: 0px; margin-bottom: 50px;">
   
  <table id="user_data" class="table table-bordered table-hover" style="background: #fff;">
      <thead class="thead-light">
        <tr>
            <th style=" text-align: center; font-size: 11px; color:#444;"> # </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> STATUS </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> BRANCH </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> VOUCHER_NO </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> VOUCHER_DATE </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> SYSTEM_DATE</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> COMPANY </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> PARTICULARS</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> VEHICLE_NO</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> AMOUNT </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> PAY_MODE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> VOUCHER_COPY </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> NARRATION </th>
        </tr>
      </thead>
  
  </table>
</div>

<style type="text/css">
  .red {
  background-color: #ffc7c7 !important;
  }
  .green {
  background-color: #e1ffe1 !important;
  } 
</style>


<script type="text/javascript">
jQuery( document ).ready(function() {

    $("#loadicon").show(); 
    var table = jQuery("#user_data").dataTable({

      "createdRow": function( row, data, dataIndex ) {
        if ( data[1] == "Rejected" ) {        
        $(row).addClass('red'); 
        }
        if ( data[1] == "Approved" ) {        
        $(row).addClass('green'); 
        }
      },

    "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
    "bProcessing": true,
      "searchDelay": 1000,

        "scrollY": 450,
        "scrollX": true,
   "sAjaxSource": "exp_reject_fetch.php?a=<?= $fromdate ?>&b=<?= $todate ?>",
    "bPaginate": true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 10,
    // "dom": "lBfrtip",
    "ordering": false,
    "buttons": [
    "copy", "excel", "print"
    ],
    "language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
        },
    dom: "<\'toolbar\'>lBfrtip", 
    "columnDefs":[
    {
    // "targets":[4],
    // "orderable":false,
    },
    ],
    "aoColumns": [
    { mData: "0" },
    { mData: "1" },
    { mData: "2" },
    { mData: "3" },
    { mData: "4" },
    { mData: "5" },
    { mData: "6" },
    { mData: "7" },
    { mData: "8" },
    { mData: "9" },
    { mData: "10" },
    { mData: "11" },
    { mData: "12" } 
    ],
    "initComplete": function( settings, json ) {
    $("#loadicon").hide();
    }
    });  

    // $("div.toolbar").html('<select class="form-control" style="width: 180px; max-height:28px; float:right; margin-left:10px;" id="table-filter"><option value="">All Voucher</option><option value="Pending">Pending</option> <option value="Approved">Approved</option><option value="Rejected">Rejected</option></select>');

    });

    $(document).ready(function() { 
    var table = $("#user_data").DataTable(); 
    $("#table-filter").on("change", function(){
    table.search(this.value).draw();   
    });
    } );    
</script>