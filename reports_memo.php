<?php

  require('connect.php');
  

   $p = $conn->real_escape_string($_REQUEST['p']);
   $f = $conn->real_escape_string($_REQUEST['f']);
   $t = $conn->real_escape_string($_REQUEST['t']);
   $f = date("Y-m-d",$f);
   $t = date("Y-m-d",$t);

	$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
  if($p!='ALL'){
	$statement = $connection->prepare("
select podmemo.*, e.name from podmemo
left join emp_attendance e on e.code = podmemo.empid
WHERE podmemo.branch='$p' and (date(podmemo.dispatchdate) BETWEEN '$f' and '$t')
GROUP by memono  order by podmemo.dispatchdate asc");
 } else {
    $statement = $connection->prepare("
select podmemo.*, e.name from podmemo
left join emp_attendance e on e.code = podmemo.empid
WHERE  (date(podmemo.dispatchdate) BETWEEN '$f' and '$t')
GROUP by memono  order by podmemo.dispatchdate asc");
 }
	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array(); 
   
  $sub_array[] = "<center> ".$sno." </center>"; 
	$sub_array[] = $row["memono"];
  $sub_array[] = date('d/m/Y', strtotime($row['memodate']));  
  $sub_array[] = $row["branch"]; 
  $sub_array[] = $row["bill_branch"]; 
  $sub_array[] = date('d/m/Y h:i:s', strtotime($row['dispatchdate']));
  $sub_array[] = $row["name"]; 
  $sub_array[] = $row["sentby"]; 

    $narration = "NA";
  if($row["sentby"]=="COURIER"){
    $narration = "Courier Name: ".$row['couriername']." / Docket No: ".$row['docketno'];
  } else if($row["sentby"]=="TRUCK"){
    $narration = "Truck No: ".$row['truckno']." / Driver Name: ".$row['drivername']." / Driver Mobile: ".$row['drivermobile'];
  } else if($row["sentby"]=="PERSON"){
    $narration = "Person Name: ".$row['contactname']." / Person Mobile: ".$row['contactmobile'];
  } else if($row["sentby"]=="OTHERS"){
    $narration = "Others: ".$row['narration'];
  }


  $sub_array[] = $narration;  

  if($row['collectdate']=="0000-00-00 00:00:00" || $row['collectdate']==NULL){
    $cold = "Not Received";
  } else {
    $cold = $row['collectdate'];
  }
  $sub_array[] = $cold; 
  $sub_array[] = $row["remainLR"]; 
  $sub_array[] = "<button onclick='view(\"".$row['memono']."\")' class='btn btn-sm btn-primary' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-list'></i> VIEW  </button>

  <button onclick='window.open(\"reports_print.php?id=".$row['memono']."\", \"_blank\");' class='btn btn-sm btn-warning' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-print'></i> PRINT  </button>"; 
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 