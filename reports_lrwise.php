<?php

  require('connect.php');
 
   $p = $conn->real_escape_string($_REQUEST['p']); 
 
$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
$statement = $connection->prepare("
  SELECT r.*,l.lrno as lr, l.date as lrdate, l.consignor, b.name as bparty, truck_no, p.dispatchdate, p.collectdate, p.reject, p.dispatchbranch, p.collectbranch, p.memono, e1.name as lruser, e2.name as poduser, e3.name as dispatchuser, e4.name as collectuser, l.branch as lrbranch FROM lr_sample l 
  left join rcv_pod r on r.lrno=l.lrno 
  left join podtrack p on p.lrid=r.id 
  left join billing_party b on b.id=r.billing_party
  left join emp_attendance e1 on e1.code = l.branch_user
  left join emp_attendance e2 on e2.code = r.branch_user
  left join emp_attendance e3 on e3.code = p.dispatchusr
  left join emp_attendance e4 on e4.code = p.collectusr
  where l.lrno ='$p'");
$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array(); 
 

 //  $btn= "<center> <div class='form-group' style='margin:0px !important;'> <input name='mark[]' type='checkbox' id='".$row["id"]."' value='".$row["id"]."'> <label for='".$row["id"]."'>   </label> </div>   </center> "; 
 //  $sub_array[] = $btn; 
	$sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = $row["frno"]!='' ? $row["frno"] : '<center><font color=red>NA</font></center>'; 
  $sub_array[] = $row["truck_no"]; 
  $sub_array[] = $row["lr"]; 
  $sub_array[] = date('d/m/Y', strtotime($row['lrdate']));
  $sub_array[] = $row["lrbranch"]; 
  $sub_array[] = $row["lruser"]!='' ? $row['lruser'] : '<center><font color=red>NA</font></center>'; 
  $sub_array[] = $row["pod_date"]!='' ? date('d/m/Y', strtotime($row['pod_date'])) : '<center><font color=red>NA</font></center>'; 
  $sub_array[] = $row["branch"]!='' ? $row["branch"] : '<center><font color=red>NA</font></center>'; 
  $sub_array[] = $row["poduser"]!='' ? $row['poduser'] : '<center><font color=red>NA</font></center>'; 

$pod_files1 = array(); 
$copy_no = 0;
  foreach(explode(",",$row['pod_copy']) as $pod_copies)
  {
    $copy_no++;
          
          if (strpos($pod_copies, 'pdf') !== false) {
          $file = 'PDF';
          } else {
          $file = 'IMAGE';
          }

      if($row['veh_type']=="MARKET"){
        $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
      } else {
        $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
      }
   }
  $sub_array[] = $row["pod_copy"]!='' ? implode(", ",$pod_files1) : '<center><font color=red>NA</font></center>'; 
  // $sub_array[] = $row["consignor"]; 
  $sub_array[] = $row["bparty"]!='' ? $row["bparty"] : '<center><font color=red>NA</font></center>'; 
  $sub_array[] = $row["billing_ofc"]!='' ? $row["billing_ofc"] : '<center><font color=red>NA</font></center>'; 
  $sub_array[] = $row["dispatchdate"]!='' ? date('d/m/Y h:i:s', strtotime($row['dispatchdate'])) : '<center><font color=red>NA</font></center>'; 
    $sub_array[] = $row["dispatchbranch"]!='' ? $row["dispatchbranch"] : '<center><font color=red>NA</font></center>'; 
    $sub_array[] = $row["dispatchuser"]!='' ? $row["dispatchuser"] : '<center><font color=red>NA</font></center>'; 
  $sub_array[] = $row["memono"]!='' ? $row["memono"] : '<center><font color=red>NA</font></center>'; 

  if($row["reject"]!='1'){
  $sub_array[] = $row["collectdate"]!='' ? date('d/m/Y h:i:s', strtotime($row['collectdate'])) : '<center><font color=red>NA</font></center>'; 
  }else {
  $sub_array[] = '<center><font color=red>Rejected</font></center>'; 

  }
  $sub_array[] = $row["collectbranch"]!='' ? $row["collectbranch"] : '<center><font color=red>NA</font></center>'; 
  $sub_array[] = $row["collectuser"]!='' ? $row["collectuser"] : '<center><font color=red>NA</font></center>'; 
 


 
  // if($row['nullify']=="1"){
  // $sub_array[] = "FOC"; 
  // } else if($row["billing_time"]=="" && $row["branch"]==$row["billing_ofc"]){
  // $sub_array[] = " POD/Billing - Same Branch"; 
  // } else if($row["billing_time"]==""){
  // $sub_array[] = "dispatch pending.."; 
  // } else {
  // $sub_array[] = $row["billing_time"]; 
  // }  


  // if($row['nullify']=="1"){
  // $sub_array[] = "FOC"; 
  // } else if($row["colset_time"]=="" && $row["branch"]==$row["billing_ofc"]){
  // $sub_array[] = " POD/Billing - Same Branch"; 
  // } else if($row["colset_time"]==""){
  // $sub_array[] = "collect pending.."; 
  // } else{
  // $sub_array[] = $row["colset_time"]; 
  // }
  // $stat = "";
  // // if($row['billing']=="" && $row['colset']=="-1"){
  // // $stat = "<font color='red'>CANCEL</font>";
  // // }else {
  // // $stat = "NA";
  // // }
  
  // $sub_array[] = $stat; 

    
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 