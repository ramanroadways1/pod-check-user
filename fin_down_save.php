<?php
require('connect.php');

function before ($thiss, $inthat)
{
    return substr($inthat, 0, strpos($inthat, $thiss));
}

function after ($thiss, $inthat)
{
    if (!is_bool(strpos($inthat, $thiss)))
        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
}


$daterange = $_POST['daterange'];


$fromdate = before('-', $daterange); $fromdate = date("Y-m-d", strtotime($fromdate)); 
$todate = after('-', $daterange); $todate = date("Y-m-d", strtotime($todate));
 
 
$date1=date_create($fromdate);
$date2=date_create($todate);
$diff = date_diff($date1,$date2);
$diff = $diff->format("%a");
if($diff>31){
  echo "<script> alert('Date difference can\'t be more than 31 days !'); </script>";
  exit();
}

$output ='';
 
$result = mysqli_query($conn,"SELECT rcv_pod.remark, rcv_pod.veh_type, freight_form_lr.crossing,rcv_pod.id,rcv_pod.ho_pod_check,freight_form_lr.fstation, freight_form_lr.tstation, lr_sample.dest_zone, rcv_pod.frno, rcv_pod.lrno, rcv_pod.branch as pod_rcvr,fm_date,fm_amount,fm_adv,fm_bal,pod_branch as fm_branch,pod_copy,pod_date, del_date,bal_date,late_charge,billing_ofc,billing_time, freight_form_lr.truck_no, freight_form_lr.consignor, freight_form_lr.consignee, freight_form_lr.wt12, freight_form_lr.weight, emps.name FROM rrpl_database.rcv_pod as rcv_pod 
        left join rrpl_database.emp_attendance as emps on emps.code = rcv_pod.branch_user

        LEFT JOIN rrpl_database.lr_sample as lr_sample on lr_sample.lrno=rcv_pod.lrno 
        left join rrpl_database.freight_form_lr on rcv_pod.frno = freight_form_lr.frno and rcv_pod.lrno=freight_form_lr.lrno
        WHERE (pod_date between '$fromdate' AND '$todate') and consignor_id='56' and rcv_pod.frno not like '___M%'
        group by rcv_pod.id
        ORDER BY rcv_pod.id ASC");

if(mysqli_num_rows($result) == 0)
{
  echo "<script type='text/javascript'>
    alert('No result found !');
    window.location.href='pod_down.php';
    </script>";
    exit();
}

 $output .= '
   <table border="1">  

            <th style=" text-align: center;  color:#444;"> POD  STATUS </th>
            <th style=" text-align: center;  color:#444;"> FREIGHT  MEMO NO </th> 
            <th style=" text-align: center;  color:#444;"> LR NO </th> 
            <th style=" text-align: center;  color:#444;"> TRUCK NO </th> 
            <th style=" text-align: center;  color:#444;"> ACTUAL WEIGHT</th>
            <th style=" text-align: center;  color:#444;"> CHARGE WEIGHT</th>
            <th style=" text-align: center;  color:#444;"> FROM  STATION</th>
            <th style=" text-align: center;  color:#444;"> TO  STATION</th>
            <th style=" text-align: center;  color:#444;"> DEST.  ZONE</th>
            <th style=" text-align: center;  color:#444;"> POD  RECEIVER</th>
            <th style=" text-align: center;  color:#444;"> POD COPY  </th>
            <th style=" text-align: center;  color:#444;"> POD DATE  </th>
            <th style=" text-align: center;  color:#444;"> CONSIGNOR </th>
            <th style=" text-align: center;  color:#444;"> CONSIGNEE </th>
            <th style=" text-align: center;  color:#444;"> FM BRANCH </th>
            <th style=" text-align: center;  color:#444;"> FM DATE </th>
            <th style=" text-align: center;  color:#444;"> FM AMOUNT </th>
            <th style=" text-align: center;  color:#444;"> FM ADVANCE </th>
            <th style=" text-align: center;  color:#444;"> FM BALANCE </th>
            <th style=" text-align: center;  color:#444;"> CROSSING </th>
            <th style=" text-align: center;  color:#444;"> DELIVERY DATE </th>
            <th style=" text-align: center;  color:#444;"> BALANCE DATE </th>
            <th style=" text-align: center;  color:#444;"> LATE CHARGE </th>
            <th style=" text-align: center;  color:#444;"> BILLING OFFICE </th>
            <th style=" text-align: center;  color:#444;"> BILLING TIME </th>
            <th style=" text-align: center;  color:#444;"> REMARK </th>

  </tr>
  ';
  while($row = mysqli_fetch_array($result))
  {

  if ($row["ho_pod_check"] == "1" ){
  $stat = "Approved";    
  $class = "style='color: green !important; text-align: left;'";
  }
  else if ($row["ho_pod_check"] == "-1" ){
  $stat = "Rejected";    
  $class = "style='color: red !important; text-align: left;'";
  } else {
  $stat = "Pending";  
  $class = "style='text-align: left;'";
  }


$pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['pod_copy']) as $pod_copies)
  {
    $copy_no++;
          
          if (strpos($pod_copies, 'pdf') !== false) {
          $file = 'PDF';
          } else {
          $file = 'IMAGE';
          }

      if($row['veh_type']=="MARKET"){
        $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a> </center>";
      } else {
        $pod_files1[] = "<center><a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
      }
   }
  $podfile = implode("",$pod_files1);

   $output .= '
        <tr> 
              <td '.$class.'>'.$stat.'</td> 
              <td '.$class.'>'.$row["frno"].'</td> 
              <td '.$class.'>'.$row["lrno"].'</td> 
              <td '.$class.'>'.$row["truck_no"].'</td> 
              <td '.$class.'>'.$row["wt12"].'</td> 
              <td '.$class.'>'.$row["weight"].'</td> 
              <td '.$class.'>'.$row["fstation"].'</td> 
              <td '.$class.'>'.$row["tstation"].'</td> 
              <td '.$class.'>'.$row["dest_zone"].'</td> 
              <td '.$class.'>'.$row["pod_rcvr"].'</td>  
              <td><span style="font-size:12px !important;">'.$podfile.'</span></td>  
              <td '.$class.'>'.$row["pod_date"].'</td>
              <td '.$class.'>'.$row["consignor"].'</td>   
              <td '.$class.'>'.$row["consignee"].'</td>   
              <td '.$class.'>'.$row["fm_branch"].'</td>   
              <td '.$class.'>'.$row["fm_date"].'</td>  
              <td '.$class.'>'.$row["fm_amount"].'</td>  
              <td '.$class.'>'.$row["fm_adv"].'</td>  
              <td '.$class.'>'.$row["fm_bal"].'</td>  
              <td '.$class.'>'.$row["crossing"].'</td>   
              <td '.$class.'>'.$row["del_date"].'</td>  
              <td '.$class.'>'.$row["bal_date"].'</td>  
              <td '.$class.'>'.$row["late_charge"].'</td>  
              <td '.$class.'>'.$row["billing_ofc"].'</td>  
              <td '.$class.'>'.$row["billing_time"].'</td>  
              <td '.$class.'>'.$row["remark"].'</td>  
        </tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  $name = "POD-UPLOADS_".$fromdate."to".$todate.".xls";
  header('Content-Disposition: attachment; filename='.$name.'');
  echo $output;
  exit();