<?php

  require('connect.php');
   
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
      $statement = $connection->prepare("SELECT *, COUNT(*) as total FROM rrpl_database.lr_mail where status='-1' and mail='0' GROUP by branch, batchid, type");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
  $sub_array[] = "<center> <button onclick='send(".$row['id'].")' class='btn btn-sm btn-success'> <i class='fa fa-envelope '></i> <b> SEND  </b> </button> </center>"; 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['batchid']); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['type']); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['branch']); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['total']); 
  $data[] = $sub_array;
} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>