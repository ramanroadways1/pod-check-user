<?php 
require('connect.php');

function before ($thiss, $inthat)
{
    return substr($inthat, 0, strpos($inthat, $thiss));
}

function after ($thiss, $inthat)
{
    if (!is_bool(strpos($inthat, $thiss)))
        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
}


$daterange = $_POST['daterange'];


$fromdate = before('-', $daterange); $fromdate = date("Y-m-d", strtotime($fromdate)); 
$todate = after('-', $daterange); $todate = date("Y-m-d", strtotime($todate));
 
 
$date1=date_create($fromdate);
$date2=date_create($todate);
$diff = date_diff($date1,$date2);
$diff = $diff->format("%a");
if($diff>31){
  echo "<script> alert('Date difference can\'t be more than 31 days !'); </script>";
  exit();
}

?>
<div class="card-body" style="padding-top: 0px; margin-bottom: 50px;">
   
  <table id="user_data" class="table table-bordered table-hover" style="background: #fff;">
      <thead class="thead-light">
        <tr>
          <th style=" text-align: center; font-size: 11px; color:#444;"> # </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> POD <br> STATUS </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> FM <br>DATE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> FREIGHT <br> MEMO NO </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> LR NO </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> TRUCK NO </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> ACTUAL WEIGHT</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> CHARGE WEIGHT</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> FROM <br> STATION</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> TO <br> STATION</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> DEST. <br> ZONE</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> POD <br> RECEIVER</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> RECEIVER <br> NAME </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> POD <br>COPY  </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> POD <br>DATE  </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> CONSIGNOR </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> CONSIGNEE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> FM <br>BRANCH </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> FM <br>AMOUNT </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> FM <br>ADVANCE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> FM <br>BALANCE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> CROSSING </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> DELIVERY <br>DATE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> BALANCE <br>DATE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> LATE <br>CHARGE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> BILLING <br>OFFICE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> BILLING <br>TIME </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> REMARK </th>
            </tr>
      </thead>
  
  </table>
</div>

<style type="text/css">
  .red {
  background-color: #ffc7c7 !important;
  }
  .green {
  background-color: #e1ffe1 !important;
  } 
</style>


<script type="text/javascript">
jQuery( document ).ready(function() {

    $("#loadicon").show(); 
    var table = jQuery("#user_data").dataTable({

      "createdRow": function( row, data, dataIndex ) {
        if ( data[1] == "Rejected" ) {        
        $(row).addClass('red'); 
        }
        if ( data[1] == "Approved" ) {        
        $(row).addClass('green'); 
        }
      },

    "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
    "bProcessing": true,
      "searchDelay": 1000,

        "scrollY": 450,
        "scrollX": true,
   "sAjaxSource": "fin_approv_fetch.php?a=<?= $fromdate ?>&b=<?= $todate ?>",
    "bPaginate": true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 10,
    // "dom": "lBfrtip",
    "ordering": false,
    "buttons": [
    // "copy", "excel", "print"
    ],
    "language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
        },
    dom: "<\'toolbar\'>lBfrtip", 
    "columnDefs":[
    { 
    },
    ], 
    "initComplete": function( settings, json ) {
    $("#loadicon").hide();
    }
    });  

    $("div.toolbar").html('<select class="form-control" style="width: 180px; max-height:28px; float:right; margin-left:10px;  margin-bottom:10px;" id="table-filter"><option value="">All POD</option><option value="Pending">Pending</option> <option value="Approved">Approved</option><option value="Rejected">Rejected</option></select>');

    });

    $(document).ready(function() { 
    var table = $("#user_data").DataTable(); 
    $("#table-filter").on("change", function(){
    table.search(this.value).draw();   
    });
    } );   
</script>