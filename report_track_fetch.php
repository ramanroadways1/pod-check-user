<?php 
require('connect.php'); 

$docketno = $conn->real_escape_string($_REQUEST['p']);

// $docketno = escapeString($conn,$_REQUEST['p']); 

$curl = curl_init(); 
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.sevasetu.in/sp/index.php/api/tracking/track_v2?username=maruticourier&password=17322463b3e529f1ee3184444b7e0d61",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS =>"{\"data\": {\"barcode_no\":\"$docketno\", \"type\":\"traveling\"}}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json"
  ),
));

$response = curl_exec($curl); 
curl_close($curl);  
$output = $response; 

$output = json_decode($output, true);

if($output['success']=="1"){

?>

<style type="text/css">
	.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444; text-transform: uppercase; } .component{display: none;} 
	table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
</style>

<div class="row">  
<div class="col-md-12" >
<div class="card shadow mb-4"> 
<div class="card-header"> 
</div>
		<div class="card-body">

		<div class="col-md-10 offset-md-1">
		<h5 style="text-align: center; padding-bottom: 10px;"> || Travelling Information ||</h5>

<?php 
	
	if($output['success']=="1"){

	$variable = $output['data']['trackinginfo'];
	$variable1 = $output['data']['trackinginfo'];

?>
		<table id="user_data" class="table table-bordered table-hover" style="">
		<thead class="thead-light">
		<tr>  
		<th> Date </th>  
		<th> In/Out Scan </th>
		<th> Travelling </th>
		<th> Date Time </th> 
		</tr>
		</thead> 
		<tbody>
			<?php 
				foreach ($variable as $key => $value) {

$date = $key;
$date1 = date('l', strtotime($date));
$date2 = date('d M Y', strtotime($date));

					 echo "<tr>";
					 echo "<td colspan='4'> ".$date2." / ".$date1." </td>";
					 echo "</tr>";
					 $newkey = $variable1[$key];
					 foreach ($newkey as $newkey) {

						if($newkey['process']=="Inscan"){

						echo "<tr>";
						echo "<td width='180px'> </td>";
						echo "<td> ".$newkey['process']." </td>";
						echo "<td> Inscan by ".$newkey['from']."</td>";
						echo "<td>".$newkey['transDateTime']."</td>";  
						echo "</tr>";

						} else if($newkey['process']=="Outscan"){

						echo "<tr>";
						echo "<td width='180px'> </td>";
						echo "<td> ".$newkey['process']." </td>";
						echo "<td> Outscan From ".$newkey['from']." to ".$newkey['to']."</td>";
						echo "<td>".$newkey['transDateTime']."</td>";  
						echo "</tr>";

						} 
					} 
				} 
			?>
		</tbody>
		</table>

<?php } ?>
	</div>

<?php 

$curl = curl_init(); 
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.sevasetu.in/sp/index.php/api/tracking/track_v2?username=maruticourier&password=17322463b3e529f1ee3184444b7e0d61",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS =>"{\"data\": {\"barcode_no\":\"$docketno\", \"type\":\"delivery\"}}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json"
  ),
));

$response = curl_exec($curl); 
curl_close($curl);
$output2 = $response;

?>

	<div class="col-md-10 offset-md-1">
		<h5 style="text-align: center; padding-bottom: 10px; padding-top: 20px;"> || Delivery Information ||</h5>

<?php 
$output2 = json_decode($output2, true);
	
	if($output2['success']=="1"){

	$variable = $output2['data']['trackinginfo'];

?>
		<table id="user_data" class="table table-bordered table-hover" style="">
		<thead class="thead-light">
		<tr>  
			<th> DELIVERY DATE </th>  
			<th> DOCUMENT NO </th>
			<th> FROM CENTER </th>
			<th> TO CENTER </th> 
			<th> REASON </th>
			<th> RECEIVER NAME </th>
			<th> RECEIVER PHONE	</th>
			<th> STATUS </th>
			<th> POD </th>
		</tr>
		</thead> 
		<tbody>
			<?php foreach ($variable as $variable) { 
			?>
			<td> <?php echo $variable['DeliveryDate']; ?> </td>
			<td> <?php echo $variable['DocumentNo']; ?> </td>
			<td> <?php echo $variable['FromCenterName']; ?> </td>
			<td> <?php echo $variable['ToCenterName']; ?> </td>
			<td> <?php echo $variable['Reason']; ?> </td>
			<td> <?php echo $variable['Receiver']; ?> </td>
			<td> <?php echo $variable['RMobile']; ?> </td>
			<td> <?php echo $variable['StatusName']; ?> </td>
			<td> <a target="_blank" href="<?php echo $variable['DrsScanImage']; ?>"> View POD </a>  </td>
			<?php } ?>
		</tbody>
		</table>

<?php } ?>
	</div>	
	</div>
</div>
</div> 
</div> 
<?php } else {

	echo "
	<script>
	Swal.fire({
	icon: 'error',
	title: 'Error !!!',
	text: 'Tracking Details Not Found...'
	})
	</script>";  

} ?>