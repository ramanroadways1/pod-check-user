<?php

  require('connect.php');
 

   // $branchuser = $conn->real_escape_string($_REQUEST['p']);

   // $p = $branchuser;
   $f = $conn_rrpl->real_escape_string($_REQUEST['f']);
   $t = $conn_rrpl->real_escape_string($_REQUEST['t']);

$fromdate = date("Y-m-d",$f);
$todate = date("Y-m-d",$t);

$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
 
  $statement = $connection->prepare("select u.username as branch, COALESCE(t1.count,0) as made,COALESCE(t2.count,0) as receive, COALESCE(t3.count,0) as self, COALESCE(t4.count,0) as pending_dispatch, COALESCE(t5.count,0) as dispatch, COALESCE(t6.count,0) as pending_collect, COALESCE(t7.count,0) as collect, COALESCE(t8.count,0) as nullify, COALESCE(t9.count,0) as memo, COALESCE(t10.count,0) as pending_tostation from user u
left join (select branch, count(id) as count from lr_sample where (date BETWEEN '$fromdate' and '$todate') GROUP by branch) t1 on t1.branch =  u.username
left join (select branch, count(id) as count from rcv_pod where (pod_date BETWEEN '$fromdate' and '$todate') GROUP by branch) t2 on t2.branch = u.username
left join (select branch, count(id) as count from rcv_pod where (pod_date BETWEEN '$fromdate' and '$todate') and self='1' and nullify='0' GROUP by branch) t3 on t3.branch = u.username
left join (select fromstation, count(id) as count from rcv_pod where (pod_date BETWEEN '$fromdate' and '$todate') and billing='' and self='0' and nullify='0' GROUP by fromstation) t4 on t4.fromstation = u.username
left join (select fromstation, count(id) as count from rcv_pod where (pod_date BETWEEN '$fromdate' and '$todate') and billing='1' and self='0' and nullify='0' GROUP by fromstation) t5 on t5.fromstation = u.username
left join (select tostation, count(id) as count from rcv_pod where (date(billing_time) BETWEEN '$fromdate' and '$todate') and colset='' and self='0' and nullify='0' and billing='1' GROUP by tostation) t6 on t6.tostation = u.username
left join (select tostation, count(id) as count from rcv_pod where (date(billing_time) BETWEEN '$fromdate' and '$todate') and colset='1' and self='0' and nullify='0' and billing='1' GROUP by tostation) t7 on t7.tostation = u.username
left join (select branch, count(id) as count from rcv_pod where (pod_date BETWEEN '$fromdate' and '$todate') and nullify!='0' GROUP by branch) t8 on t8.branch = u.username
left join (select branch, count(id) as count from podmemo where (memodate BETWEEN '$fromdate' and '$todate') GROUP by branch) t9 on t9.branch = u.username
left join (select fromstation, count(id) as count from rcv_pod where (pod_date BETWEEN '$fromdate' and '$todate') and billing='' and self='0' and nullify='0' and tostation='' GROUP by fromstation) t10 on t10.fromstation = u.username
where u.role='2' and u.balance!='0' HAVING (made!='0' and receive!='0')");
 
$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array(); 
 

 //  $btn= "<center> <div class='form-group' style='margin:0px !important;'> <input name='mark[]' type='checkbox' id='".$row["id"]."' value='".$row["id"]."'> <label for='".$row["id"]."'>   </label> </div>   </center> "; 
 //  $sub_array[] = $btn; 
	$sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = $row["branch"]; 
  $sub_array[] = $row["made"]; 
  $sub_array[] = $row["receive"]; 
  $sub_array[] = $row["nullify"]; 
  $sub_array[] = $row["self"];   
  $sub_array[] = $row["pending_dispatch"]; 
  $sub_array[] = $row["dispatch"]; 
  $sub_array[] = $row["pending_tostation"]; 
  $sub_array[] = $row["pending_collect"]; 
  $sub_array[] = $row["collect"]; 
  $sub_array[] = $row["memo"]; 
  
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 