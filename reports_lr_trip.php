<?php

  require('connect.php');
 
     $branchuser = $conn->real_escape_string($_REQUEST['p']);


   // $p = $branchuser;
   // $f = $conn_rrpl->real_escape_string($_REQUEST['f']);
//    $t = $conn_rrpl->real_escape_string($_REQUEST['t']);

// $f = date("Y-m-d",$f);
// $t = date("Y-m-d",$t);

$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );

if($branchuser!='ALL'){
  $statement = $connection->prepare("
  select o.*, datediff(curdate(),duedate) as diff from dairy.opening_closing o where fromstation='$branchuser' and closing_date!='0000-00-00' and hisab_sent='0' and nullify='0'");
} else {
   $statement = $connection->prepare("
  select o.*, datediff(curdate(),duedate) as diff from dairy.opening_closing o where closing_date!='0000-00-00' and hisab_sent='0' and nullify='0'");
}

$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array(); 
 

 //  $btn= "<center> <div class='form-group' style='margin:0px !important;'> <input name='mark[]' type='checkbox' id='".$row["id"]."' value='".$row["id"]."'> <label for='".$row["id"]."'>   </label> </div>   </center> "; 
 //  $sub_array[] = $btn; 
	$sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = $row["trip_no"]; 
  $sub_array[] = $row["tno"]; 
  $sub_array[] = $row["avg"]; 
  $sub_array[] = $row["opening_branch"]; 
  $sub_array[] = date('d/m/Y', strtotime($row['opening_date']));
  $sub_array[] = $row["closing_branch"]; 
  $sub_array[] = date('d/m/Y', strtotime($row['closing_date']));
  $sub_array[] = date('d/m/Y', strtotime($row['duedate']));
  
  $sub_array[] = $row["diff"]." days"; 
  
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 