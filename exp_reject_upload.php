<?php 
 
	require('connect.php');

	function sanitize($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}  

 	$id =  $conn -> real_escape_string($_POST['post_id']); 

	$file_name = $_FILES['file']['name'];
	$file_size =$_FILES['file']['size'];
	$file_tmp =$_FILES['file']['tmp_name'];
	$file_type=$_FILES['file']['type'];
	@$file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));

	$uploadid = strtoupper(chr(rand(65, 90)) . strtotime('now') . chr(rand(97,122)));
 
	$getname = substr(preg_replace("/[^0-9a-zA-Z]/", "", $_FILES['file']['name']), 0, -4);	
	$allowedExts = array("gif", "jpeg", "jpg", "png", "pdf"); 
	$temp = explode(".", $_FILES["file"]["name"]); 
	$extension = end($temp);
 
	if ((($_FILES["file"]["type"] == "image/gif")
	|| ($_FILES["file"]["type"] == "image/jpeg")
	|| ($_FILES["file"]["type"] == "image/jpg")
	|| ($_FILES["file"]["type"] == "image/pjpeg")
	|| ($_FILES["file"]["type"] == "image/x-png")
	|| ($_FILES["file"]["type"] == "image/png")
	|| ($_FILES["file"]["type"] == "application/pdf") 
	&& in_array($extension, $allowedExts))){

	$newfile = $getname.$uploadid.".".$file_ext;	
	$targetPath =  '../b5aY6EZzK52NA8F/upload/'.$newfile; 

	if(move_uploaded_file($file_tmp, $targetPath)){
 
	try {
			$conn->query("START TRANSACTION"); 

			$sql = "select * from rrpl_database.mk_venf where id='$id'";
			if ($conn->query($sql) === FALSE) {
				$errorno = mysqli_error($conn);
				throw new Exception($errorno); 
			} 
			$resg = $conn->query($sql);
			$rowg = $resg->fetch_assoc();
 
			$sql = "insert into exp_upload_log (vid,voucher,oldupload,newupload) values ('$rowg[id]','$rowg[vno]','$rowg[upload]','upload/$newfile')";
 			if ($conn->query($sql) === FALSE) {
				$errorno = mysqli_error($conn);
				throw new Exception($errorno); 
			}

			$sql = "update rrpl_database.mk_venf set upload='upload/$newfile', approval='1' where id='$id'";	 
			if ($conn->query($sql) === FALSE) {
				$errorno = mysqli_error($conn);
				throw new Exception($errorno); 
			} 
 
			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Image Updated.',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

	} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
	}            
	} else {
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Uploading Error - Something went wrong !'
			})
			</script>";		
	}
	} else {
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Please upload a valid image file !'
			})
			</script>";	
	}
 
?>  