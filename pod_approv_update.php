<?php
 
 require('connect.php');
 
    $id = $conn -> real_escape_string($_POST['id']);
    $stat = $conn -> real_escape_string($_POST['stat']);
   
   $status="";
   if($stat=="1"){
   	$status="icon: 'success',title: 'POD Approved',";
   }else if($stat=="-1"){
   	$status="icon: 'error',title: 'POD Rejected',";
   }

	$chk_claim = mysqli_query($conn, "SELECT branch,claim_ho FROM rrpl_database.rcv_pod WHERE id = '$id'");
	
	if(!$chk_claim){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#ApprovalSelectBox$id').val('0'); </script>";
		exit();
	} 
	
	$row_claim = fetchArray($chk_claim);
	
	if($row_claim['claim_ho']=="0")
	{
		echo "<script>
			Swal.fire({
			position: 'top-end',
			icon: 'error',title: 'Update claim first !',
			showConfirmButton: false,
			timer: 1000
		})
		$('#loadicon').hide(); 
		$('#ApprovalSelectBox$id').val('0');
		</script>";
		exit();
	}
	
	$qry = mysqli_query($conn, "UPDATE rrpl_database.rcv_pod SET reupload='0', ho_pod_check = '$stat', ho_pod_check_time=now() 
	WHERE id = '$id'");
	
	if(!$qry){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#ApprovalSelectBox$id').val('0'); </script>";
		exit();
	} 

	$sqlm = "SELECT email FROM rrpl_database.user where role='2' and username='$row_claim[branch]'";
	$resm = $conn->query($sqlm);
	$rowm = $resm->fetch_assoc();

	$qry = mysqli_query($conn, "insert into rrpl_database.lr_mail (status, branch, emailid, batchid, type, refid) values ('1', '$row_claim[branch]', '$rowm[email]','$batchid', 'LR', '$id')");
	
	if(!$qry){
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error');$('#loadicon').hide(); $('#ApprovalSelectBox$id').val('0'); </script>";  
		exit();
	} 
	
	echo "<script>
		Swal.fire({
		position: 'top-end',
		".$status."
		showConfirmButton: false,
		timer: 1000
	})
	$('#loadicon').hide(); 
    $('#user_data').DataTable().ajax.reload(null, false); 
	</script>";
				

?> 