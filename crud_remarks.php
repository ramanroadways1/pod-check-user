<?php require('header.php');
 
?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<style type="text/css">
	.applyBtn{
		border-radius: 0px !important;
	}
	.show-calendar{
		top: 180px !important;
	} 
    .applyBtn{
        border-radius: 0px !important;
    }
    table.table-bordered.dataTable td {
        padding: 10px 5px 10px 10px;
    }
    .dt-buttons{float: right;}
    .user_data_filter{
        float: right;
    }

    .dt-button {
        padding: 4px 20px;
        text-transform: uppercase;
        font-size: 11px;
        text-align: center;
        cursor: pointer;
        outline: none;
        color: #fff;
        background-color: #37474f ;
        border: none;
        border-radius:  2px;
        box-shadow: 0 3px #999;
    }


    .dt-button:hover {background-color: #3e8e41}

    .dt-button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    #user_data_wrapper{
        width: 100% !important;
    }
    .dt-buttons{
        margin-bottom: 20px;
    }
 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444; text-transform: uppercase; } .component{display: none;} 
	table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
</style>

<div class="col-md-12"> <h3 style="float: left;">Manage Remarks </h3> <span style="float: right;"> <button class='btn btn-sm btn-primary' onclick="add()"> <i class='fa fa-plus-square '></i> <b> &nbsp;  ADD NEW REMARK </b> </button> </span> </div>

 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
 </div>

<div id="response"></div>
 <div class="col-md-12 table-responsive">
   <table id="user_data" class="table table-bordered table-hover" style="background-color: #fff;">
      <thead class="thead-light">
        <tr>
        <th width="200px"> ACTION  </th> 
        <th width="150px"> ID  </th> 
        <th> TITLE </th> 
        </tr>
      </thead> 
  </table>
</div>
  <script type="text/javascript">
	jQuery( document ).ready(function() {

	$('#loadicon').show(); 
	var table = jQuery('#user_data').dataTable({ 
	"lengthMenu": [ [7, 500, 1000, -1], [7, 500, 1000, "All"] ], 
	"bProcessing": false,
	"sAjaxSource": "crud_items_fetch.php",
	"bPaginate": true,
	"sPaginationType":"full_numbers",
	"iDisplayLength": 7,
	"order": [[ 1, "desc" ]], 
    "dom": 'lBfrtip',
    "ordering": true,
    "buttons": [
        'copy', 'csv', 'excel', 'print'
    ],
	"aaSorting": [],
	"initComplete": function( settings, json ) {
	$('#loadicon').hide();
	}
	});  

	}); 
  </script>
 
  <script type="text/javascript">
    function update(val){
    $('#loadicon').show(); 
    var id = val;  
           $.ajax({  
                url:"crud_items_update.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#modaldetail').html(data);  
                     $('#exampleModal').modal("show");  
                     $('#loadicon').hide(); 
                }  
           });
    }

    $(document).on('submit', '#updatereq', function()
    {  
    $('#loadicon').show(); 
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'crud_items_update_save.php',
        data : data,
        success: function(data) {  
        document.getElementById("hidemodal").click();  
        $('#response').html(data);  
        $('#user_data').DataTable().ajax.reload();  
        $('#loadicon').hide(); 
        }
      });
      return false;
    });
 
    function add(val){
    $('#loadicon').show(); 
    var id = val;  
           $.ajax({  
                url:"crud_items_add.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#modaldetail').html(data);  
                     $('#exampleModal').modal("show");  
                     $('#loadicon').hide(); 
                }  
           });
    }

    $(document).on('submit', '#addreq', function()
    {  
    $('#loadicon').show(); 
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'crud_items_add_save.php',
        data : data,
        success: function(data) {  
        document.getElementById("hidemodal").click();  
        $('#response').html(data);  
        $('#user_data').DataTable().ajax.reload();  
        $('#loadicon').hide(); 
        }
      });
      return false;
    });
  </script>
   
  <?php include('footer.php'); ?>