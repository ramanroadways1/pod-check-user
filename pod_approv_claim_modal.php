<?php
	require('connect.php'); 
   
	$id = $conn->real_escape_string($_POST['id']);
 
 	$qry = mysqli_query($conn, "SELECT lrno,claim_branch,claim_ho,frno,veh_type FROM rrpl_database.rcv_pod WHERE id = '$id'");
	
	if(!$qry){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		exit();
	}
	
	if(mysqli_num_rows($qry)==0)
	{
		echo "<script> alert('ERROR: POD not found..'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		exit();
	}
	
	$row = mysqli_fetch_array($qry);
	
	if($row['claim_ho']=="1")
	{
		echo "<script> alert('ERROR: Claim already updated..'); $('#loadicon').hide();$('#ClaimSel$id').val('1');</script>";  
		exit();
	}
	
/** add only if balance pending ***/	
	
	// $qry2 = mysqli_query($conn, "SELECT paidto FROM rrpl_database.freight_form WHERE frno = '$row[frno]'");
	
	// if(!$qry2){ 
		// $error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		// echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		// exit();
	// }
	
	// if(mysqli_num_rows($qry2)==0 AND $row['veh_type']=='MARKET')
	// {
		// echo "<script> alert('ERROR: Freight memo not found..'); $('#loadicon').hide();$('#ClaimSel$id').val('');</script>";  
		// exit();
	// }
	
	// $row2 = mysqli_fetch_array($qry2);
	
	// if($row2['paidto']!='' AND $row['veh_type']=='MARKET')
	// {
		// echo "<script>
			// Swal.fire({
				// position: 'top-end',
				// icon: 'error',
				// title: 'Balance paid',
				// showConfirmButton: false,
				// timer: 1000
			// })
			// $('#ClaimSel$id').val('');
			// $('#loadicon').hide();
		// </script>";
		// exit();
	// }
	
/** add only if balance pending ***/	
?>
<style>
#label_modal{font-size:12px;}
</style> 

<form method="post" action="" id="ClaimForm" role="form" autocomplete="off">
<div class="modal-body">
	
	<div class="row">
		<div class="col-md-3">
			<button class="btn btn-success" type="button" onclick="AddNewClaimRecord('<?php echo $id; ?>')"> 
				ADD Claim
			</button>
		</div>	
		
		<div class="col-md-6"></div>
		
		<div class="col-md-3">
			Total Claim : <span style="color:red;font-weight:bold;letter-spacing:1px" id="total_claim_amount_html"></span>
		</div>	
	</div>

	<div class="row" id="result_modal_claim"></div>

</div>

<div id="result_add_new_record"></div>

<input type="hidden" name="total_claim_amount_db" id="total_claim_amount_db">
<input type="hidden" name="lrno" value="<?php echo $row['lrno']; ?>">
	<input type="hidden" name="pod_id" value="<?php echo $id; ?>">
	<input type="hidden" name="vou_no" value="<?php echo $row['frno']; ?>">
	<div class="modal-footer">
        <button type="submit" disabled id="btn_claim_submit" class="btn btn-primary">Submit</button>
        <button type="button" onclick="$('#ClaimSel<?php echo $id; ?>').val('')" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
	  
</form> 

<script>
function AddNewClaimRecord(id)
{
	$('#total_claim_amount_html').html('0');
	$.ajax({  
     url:"pod_approv_claim_modal_add_new_record.php",  
     method:"post",  
     data:{id:id},  
     success:function(data){  
        $('#result_add_new_record').html(data);  
     }});
}

function LoadClaimData(id)
{
	$.ajax({  
     url:"pod_approv_claim_modal_load_claim.php",  
     method:"post",  
     data:{id:id},  
     success:function(data){  
        $('#result_modal_claim').html(data);  
     }});
}	

function DeleteClaim(id,id2)
{
	$('#total_claim_amount_html').html('0');
	$.ajax({  
     url:"pod_approv_claim_modal_remove_claim.php",  
     method:"post",  
     data:'id=' + id + '&id2=' + id2,  
     success:function(data){  
        $('#result_add_new_record').html(data);  
     }});
}

LoadClaimData('<?php echo $id; ?>');
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#ClaimForm").on('submit',(function(e) {
$('#btn_claim_submit').attr('disabled',true);	
$("#loadicon").show();
e.preventDefault();
	$.ajax({
	url: "./pod_approv_save_claim.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_add_new_record").html(data);
	},
	error: function() 
	{} });}));});
</script>