<?php
require('connect.php'); 
$id = $conn->real_escape_string($_POST['id']);
?>
<style>
#label_modal{font-size:12px;}
</style> 

<div class="modal-body">
	<div class="row">
<?php
$qry = mysqli_query($conn, "SELECT * FROM rrpl_database.claim_records_admin_desc WHERE pod_id = '$id'");
	
	if(!$qry){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#claimModal').modal('show');  </script>";  
		exit();
	}
	
	if(mysqli_num_rows($qry)==0)
	{
		?><div class="col-md-12" style="color:Red">No record added yet !!</div><?php
	}
	else
	{
	?>
	
	<div class="col-md-12">
		<table class="table table-bordered table-striped">
		<tr style="font-size:13px">
			<th>#</th>
			<th>Claim Type</th>
			<th>Claim Amount</th>
			<th>Claim Unit</th>
			<th>Unit Value</th>
			<th>Narration</th>
		</tr>
	<?php
	$sn=1;
	$total_claim_db_html=0;
	while($row = mysqli_fetch_array($qry))
	{
		$total_claim_db_html = $total_claim_db_html+$row['amount'];
		echo "<tr>
			<td>$sn</td>
			<td>$row[claim_type]</td>
			<td>$row[amount]</td>
			<td>$row[claim_unit]</td>
			<td>$row[unit_value]</td>
			<td>$row[narration]</td>
		</tr>";
	$sn++;	
	}
?>
	<tr>
		<td colspan="2">Total :</td>
		<td><?php echo "<b>$total_claim_db_html/-</b>"; ?></td>
		<td colspan="3"></td>
	</tr>
		</table>
	</div>
	<?php
	}
	?>

	</div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<script>
$('#claimViewModal').modal('show');
$('#loadicon').hide();
</script>