<?php
require('connect.php');
 
$output ='';
 
$result = mysqli_query($conn,"SELECT * FROM rrpl_database.mk_venf WHERE approval='-1' and remark is not null");

if(mysqli_num_rows($result) == 0)
{
	echo "<script type='text/javascript'>
		alert('No result found !');
		window.location.href='exp_down.php';
		</script>";
		exit();
}

 $output .= '
   <table border="1">  

             <th style=" text-align: center;  color:#444;"> STATUS </th>
            <th style=" text-align: center;  color:#444;"> BRANCH </th> 
            <th style=" text-align: center;  color:#444;"> VOUCHER_NO </th> 
            <th style=" text-align: center;  color:#444;"> VOUCHER_DATE </th> 
            <th style=" text-align: center;  color:#444;"> SYSTEM_DATE</th>
            <th style=" text-align: center;  color:#444;"> COMPANY </th>
            <th style=" text-align: center;  color:#444;"> PARTICULARS</th>
            <th style=" text-align: center;  color:#444;"> VEHICLE_NO</th>
            <th style=" text-align: center;  color:#444;"> AMOUNT </th>
            <th style=" text-align: center;  color:#444;"> PAY_MODE </th>
            <th style=" text-align: center;  color:#444;"> VOUCHER_COPY </th> 
            <th style=" text-align: center;  color:#444;"> NARRATION </th>
            <th style=" text-align: center;  color:#444;"> REMARK </th>

	</tr>
  ';
  while($row = mysqli_fetch_array($result))
  {

	if ($row["approval"] == "1" ){
	$stat = "Approved";    
	$class = "style='color: green !important; text-align: left;'";
	}
	else if ($row["approval"] == "-1" ){
	$stat = "Rejected";    
	$class = "style='color: red !important; text-align: left;'";
	} else {
	$stat = "Pending";  
	$class = "style='text-align: left;'";
	}

$pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['upload']) as $pod_copies)
{
$copy_no++;
$pod_files1[] = "<center><a style='color: #000;' href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>Upload: $copy_no</a></center>";
}
$filesname = implode("",$pod_files1);


   $output .= '
				<tr> 
							<td '.$class.'>'.$stat.'</td> 
							<td '.$class.'>'.$row["user"].'</td> 
							<td '.$class.'>'.$row["vno"].'</td> 
							<td '.$class.'>'.$row["newdate"].'</td> 
							<td '.$class.'>'.$row["date"].'</td> 
							<td '.$class.'>'.$row["comp"].'</td> 
							<td '.$class.'>'.$row["des"].'</td>  
							<td '.$class.'>'.$row["vehno"].'</td>  
							<td '.$class.'>'.$row["amt"].'</td>  
							<td '.$class.'>'.$row["chq"].'</td>  
 							<td '.$class.'><span style="">'.$filesname.'</span></td>  
              <td '.$class.'>'.$row["narrat"].'</td> 
              <td '.$class.'>'.$row["remark"].'</td> 
				</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  $name = "VOUCHER-UPLOADS_REJECTED.xls";
  header('Content-Disposition: attachment; filename='.$name.'');
  echo $output;
  exit();