<?php 
  include_once 'header.php';
?>  

  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> -->
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
 
 <style type="text/css">
#user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color: rgb(233, 236, 239)}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:none!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444} .content{padding-bottom: 0px !important;}
</style>

<div id="response"></div>

<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
</div>  -->


<div id="dataModal" class="modal fade in"  data-keyboard="false" data-backdrop="static">  
      <div class="modal-dialog">  
           <div class="modal-content" id="dataModal_detail">   
           </div>  
      </div>  
</div>    

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">

<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 

<div class="col-md-12" >
<div class="card shadow mb-4" style="border-radius: 0px !important;"> 
<div class="card-header">
 
<div class="col-md-12" style="padding-top: 5px; padding-bottom: 10px;">
   <h6>REJECTED Expense Voucher - UPLOADS APPROVAL </h6> 
</div>
  
 
<style type="text/css">
#appenddiv2 {
    display: block; 
    position:relative
} 
</style>

 

 <div class="card-body" style="padding-top: 0px; margin-bottom: 50px;">
   
  <table id="user_data" class="table table-bordered table-hover" style="background: #fff;">
      <thead class="thead-light">
        <tr>
            <th style=" text-align: center; font-size: 11px; color:#444;"> # </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> STATUS </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> REMARK </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> BRANCH </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> VOUCHER_NO </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> VOUCHER_DATE </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> SYSTEM_DATE</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> COMPANY </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> PARTICULARS</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> VEHICLE_NO</th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> AMOUNT </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> PAY_MODE </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> VOUCHER_COPY </th> 
            <th style=" text-align: center; font-size: 11px; color:#444;"> NARRATION </th>
            <th style=" text-align: center; font-size: 11px; color:#444;"> REMARK </th>
        </tr>
      </thead>
  
  </table>
</div>

<style type="text/css">
  .red {
  /*background-color: #ffc7c7 !important;*/
  }
  .green {
  /*background-color: #e1ffe1 !important;*/
  } 
</style>


<script type="text/javascript">
jQuery( document ).ready(function() {

    $("#loadicon").show(); 
    var table = jQuery("#user_data").dataTable({

      "createdRow": function( row, data, dataIndex ) {
        if ( data[1] == "Rejected" ) {        
        $(row).addClass('red'); 
        }
        if ( data[1] == "Approved" ) {        
        $(row).addClass('green'); 
        }
      },

    "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
    "bProcessing": true,
      "searchDelay": 1000,

        "scrollY": 450,
        "scrollX": true,
   "sAjaxSource": "exp_reject_fetch.php",
    "bPaginate": true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 10,
    // "dom": "lBfrtip",
    "ordering": false,
 "buttons": [ 
              {
                  text: "DOWNLOAD",
                  action: function ( e, dt, node, config ) {
          if (confirm("Are you sure you want to Download ?")) {
            Download();
            // alert("'.$downloadid.'");
          } else {
            // alert("error");
          }
                  }
              }
          ],
    
    "language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
        },
    dom: "<\'toolbar\'>lBfrtip", 
    "columnDefs":[
    {
    // "targets":[4],
    // "orderable":false,
    },
    ],
    // "aoColumns": [
    // { mData: "0" },
    // { mData: "1" },
    // { mData: "2" },
    // { mData: "3" },
    // { mData: "4" },
    // { mData: "5" },
    // { mData: "6" },
    // { mData: "7" },
    // { mData: "8" },
    // { mData: "9" },
    // { mData: "10" },
    // { mData: "11" },
    // { mData: "12" } 
    // ],
    "initComplete": function( settings, json ) {
    $("#loadicon").hide();
    }
    });  

    // $("div.toolbar").html('<select class="form-control" style="width: 180px; max-height:28px; float:right; margin-left:10px;" id="table-filter"><option value="">All Voucher</option><option value="Pending">Pending</option> <option value="Approved">Approved</option><option value="Rejected">Rejected</option></select>');

    });

    $(document).ready(function() { 
    var table = $("#user_data").DataTable(); 
    $("#table-filter").on("change", function(){
    table.search(this.value).draw();   
    });
    } );   




    function Download(){
      window.location.href="exp_reject_download.php"; 
    } 
</script>


<!-- <form method="post" action="" id="getPAGE" autocomplete="off" enctype='multipart/form-data'> 
<div class="col-md-12" >
<div class="card-body "  style="background-color: #fff; border: 1px solid #ccc;">

  <div class="row">
 
      

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

                                        <?php //echo date('m/01/Y')." - ".date('m/t/Y'); ?>
                                        
    <div class="col-md-4">
      <label style="text-transform: uppercase;">  DATE RANGE </label>
      <input type="text" name="daterange" class="form-control" value="" required="required" />
      <input type="hidden" id="fromdate" name="fromdate">
      <input type="hidden" id="todate" name="todate"> 
    </div>

                  
                  <script type="text/javascript">
                    $(function() {
  $('input[name="daterange"]').daterangepicker({
    // minDate: '2019-09-15',
    opens: 'left'
  }, function(start, end, label) {
    document.getElementById('fromdate').value=start.format('YYYY-MM-DD');
    document.getElementById('todate').value=end.format('YYYY-MM-DD'); 
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
                  </script>                      

    <div class="col-md-2"> 
      <button type="submit" class='btn btn-primary btn-sm' style="height: 35px; margin-top:23px;"> <i class='fa fa-search '></i> <b> SEARCH  </b> </button>    
      
    </div>

                                        <div class="col-md-3">
                                            <div id="employee_detail" style="padding-top: 4px;"></div>

                                        </div>
  </div>
</div>
</form>  -->

<br> 
<div id="getPAGEDIV"></div>
</div> 
</div>
</div> 
</div>
</div> 
</div>
</div>

<script>
    $(document).on('submit', '#getPAGE', function()
    {
        // if(){
        // $('#submitbtn').attr('disabled', true);
        var data = $(this).serialize();
        $.ajax({
            type : 'POST',
            url  : 'exp_reject_load.php',
            data : data,
            success: function(data) {
                $('#getPAGEDIV').html(data);
            }
        });
        return false;
        // }

    });


    function update(val, stat){
    $('#loadicon').show(); 
    var id = val;  
    var stat = stat;  

          if(stat=="1"){
            $.ajax({  
                url:"exp_approv_update.php",  
                method:"post",  
                data:{id:id,stat:stat},  
                success:function(data){  
                     $('#response').html(data);  
                     $('#loadicon').hide(); 
                     $('#user_data').DataTable().ajax.reload(null, false); 
                }  
            }); 
          } else if(stat=="-1"){
            $('#loadicon').hide(); 
            modalup(id);
          } else {
            $('#loadicon').hide(); 
            alert('Please Approve or Reject');
          }

    }

    function modalup(val){
    $('#loadicon').show(); 
    var id = val;  
           $.ajax({  
                url:"exp_approv_modal.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#dataModal_detail').html(data);  
                     $('#dataModal').modal("show");  
                     $('#loadicon').hide(); 
                }  
           });
    }


  function showme(val){
    $('#loadicon').show(); 
    var id = val;  
           $.ajax({  
                url:"exp_reject_modal.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#dataModal_detail').html(data);  
                     $('#dataModal').modal("show");  
                     $('#loadicon').hide(); 
                }  
           });
    }

     $(document).on('submit', '#modalsave', function()
      {
        $('#loadicon').show(); 
        $('#donebtn').attr('disabled', true); 
        var data = $(this).serialize(); 
        $.ajax({
          type : 'POST',
          url  : 'exp_approv_mail.php',
          data : data,
          success: function(data) {
            $('#dataModal').modal("hide");  
            $('#user_data').DataTable().ajax.reload(null, false); 
            $('#loadicon').hide(); 
            $('#response').html(data);  
          }
        });
        return false;
      });
  
 function closemodal(){
 $('#user_data').DataTable().ajax.reload(null, false); 
 }


  $(document).ready(function()
  { 
    $(document).on('submit', '#upl52', function()
    {  
      $('#loadicon').show(); 
      $('#uplbtn3').attr('disabled', true); 
      var data = new FormData($("#upl52")[0]);
      $.ajax({  
        type : 'POST',
        url  : 'exp_reject_upload.php',
        data : data,
        processData: false,
        contentType: false,
        success: function(data) {  
        $("#upl52")[0].reset();   
          $("#response").html(data);
          $('#user_data').DataTable().ajax.reload(null, false); 
          $('#loadicon').hide();
          $('#uplbtn3').attr('disabled', false); 
          $('#dataModal').modal("hide");  
        }
      });
      return false;
    });  
  }); 
</script> 
<?php 
  include_once 'footer.php';
?>  
 