<?php
require('connect.php'); 
$timestamp = date("Y-m-d H:i:s");
 
	$id = $conn->real_escape_string($_POST['id']);
 
 $dlt_old_cache = mysqli_query($conn, "DELETE FROM rrpl_database.claim_cache_admin WHERE pod_id!='$id'");
	
	if(!$dlt_old_cache){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#claimModal').modal('show');  </script>";  
		exit();
	}
	
	$chk_branch_claim = mysqli_query($conn, "SELECT id FROM rrpl_database.claim_records_desc WHERE pod_id = '$id'");
	
	if(!$chk_branch_claim){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#claimModal').modal('show');  </script>";  
		exit();
	}
	
	if(mysqli_num_rows($chk_branch_claim)>0)
	{
		$dlt_old = mysqli_query($conn, "DELETE FROM rrpl_database.claim_cache_admin WHERE pod_id = '$id' AND ho_claim!='1'");
	
		if(!$dlt_old){ 
			$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
			echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#claimModal').modal('show');  </script>";  
			exit();
		}
	
		$copy_records = mysqli_query($conn,"INSERT INTO rrpl_database.claim_cache_admin(pod_id,claim_type,claim_amount,
		claim_unit,unit_value,narration,timestamp) SELECT '$id',claim_type,amount,claim_unit,unit_value,narration,'$timestamp' 
		FROM rrpl_database.claim_records_desc WHERE pod_id = '$id'");
		
		if(!$copy_records){ 
			$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
			echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#claimModal').modal('show');  </script>";  
			exit();
		}
	}
	
 	$qry = mysqli_query($conn, "SELECT * FROM rrpl_database.claim_cache_admin WHERE pod_id = '$id'");
	
	if(!$qry){ 
		$error = preg_replace('/[^\da-z ]/i', '', mysqli_error($conn));
		echo "<script> alert('ERROR: $error'); $('#loadicon').hide();$('#claimModal').modal('show');  </script>";  
		exit();
	}
	
	if(mysqli_num_rows($qry)==0)
	{
		?><div class="col-md-12" style="color:Red">No record added yet !!</div><?php
	}
	
	$total_claim_db_html=0;
	while($row = mysqli_fetch_array($qry))
	{
		
		$total_claim_db_html = $total_claim_db_html+$row['claim_amount'];
?>
	<div class="col-md-2">
		<label id="label_modal">Claim type <font color="red">*</font></label>
		<select style="font-size:13px" class="form-control" name="claim_type[]" required="required">
			<option value="">--Select Type--</option>
			<option <?php if($row['claim_type']=='Damage'){ echo "selected"; } ?> value="Damage">Damage (डैमेज)</option>
			<option <?php if($row['claim_type']=='Shortage'){ echo "selected"; } ?> value="Shortage">Shortage (शोर्टेज)</option>
		</select>
	</div>

	<div class="col-md-2">
		<label id="label_modal">Claim amount <font color="red">*</font></label>
		<input type="number" value="<?php echo $row['claim_amount']; ?>" min="0" oninput="FuncCalc()" name="claim_amount[]" class="form-control claim_amount_class" required>
	</div>
	
	<div class="col-md-3">
		<label id="label_modal">Unit (किलो/बैग/नंग)<font color="red">*</font></label>
		<select style="font-size:13px" onchange="UnitSel(this.value,'<?php echo $row["id"]; ?>')" class="form-control" name="claim_unit[]" required="required">
			<option value="">--Select Type--</option>
			<option <?php if($row['claim_unit']=='Kg'){ echo "selected"; } ?> value="Kg">Kg (किलो)</option>
			<option <?php if($row['claim_unit']=='Bag'){ echo "selected"; } ?> value="Bag">Bag (बैग)</option>
			<option <?php if($row['claim_unit']=='Nos'){ echo "selected"; } ?> value="Nos">Nos (नंग)</option>
		</select>
	</div>
		
<script>		
function UnitSel(elem,id1)
{
	if(elem=='Kg')
	{
		$('#unit_html_'+id1).html('(in Kg)');
	}
	else if(elem=='Bag')
	{
		$('#unit_html_'+id1).html('(No of Bags)');
	}
	else if(elem=='Nos')
	{
		$('#unit_html_'+id1).html('(No of Nos)');
	}
	else
	{
		$('#unit_html_'+id1).html('');
	}
}
</script>		
		
<script>
function FuncCalc()
{
	var sum = 0; 
	$(".claim_amount_class").each(function(){
		sum += +$(this).val();
	});
	$("#total_claim_amount_html").html(sum);
	// $("#total_claim_amount_html").attr('style','font-weight:bold;color:red;letter-spacing:1px');
	$("#total_claim_amount_db").val(sum);
}
</script>
		
	<div class="col-md-2">
		<label id="label_modal">Unit value/qty <font color="red">* <sup><span id="unit_html_<?php echo $row["id"]; ?>"></span></sup></font></label>
		<input value="<?php echo $row['unit_value']; ?>" style="font-size:12px" type="text" placeholder="No. of bag/nos etc.." oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" class="form-control" name="unit_value[]" required />
	</div>
	
	<div class="col-md-2">
		<label id="label_modal">Narration <font color="red">*</font></label>
		<textarea style="font-size:12px" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,/]/,'')" class="form-control" name="narration[]" required><?php echo $row['narration']; ?></textarea>
	</div>
	
	<div class="col-md-1">
		<label>&nbsp;</label>
		<br />
		<button class="btn btn-sm btn-danger" type="button" onclick="DeleteClaim('<?php echo $row["id"]; ?>','<?php echo $id; ?>')"><i class="fa fa-window-close"></i></button>
	</div>
<?php
	}
?>
<script>
$('#btn_claim_submit').attr('disabled',false);  
$('#total_claim_amount_html').html('<?php echo $total_claim_db_html; ?>');
$('#total_claim_amount_db').val('<?php echo $total_claim_db_html; ?>');
$('#claimModal').modal('show');  
$('#loadicon').hide();  
</script>