<?php require('header.php'); ?>

<div class="row">  
<div class="col-md-12" >
<div class="card shadow mb-4"> 
<div class="card-header"> 
</div>
		<div class="card-body" style="padding: 30px;">

		<form method="post" action="" id="track" autocomplete="off">
		<div class="row">
			<div class="col-md-4">
				<h5> Shree Maruti Courier Tracking </h5>
			</div>
			<div class="col-md-3">
				<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="p" name="p" value="" style="text-transform: uppercase; margin-top: 4px;" required="" />
			</div>
			<div class="col-md-2">
				<button class="btn btn-primary" style="margin: 0px;"> TRACK YOUR SHIPMENT </button>
			</div>
		</div> 
		</form>
		</div>
	</div>
</div>
</div>

<div id="fetch"></div>

<script type="text/javascript"> 
  $(document).on('submit', '#track', function()
    {   
      $('#loadicon').show();
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'report_track_fetch.php',
        data : data,
        success: function(data) {       
        $('#fetch').html(data);  
      	$('#loadicon').hide();
        }
      });
      return false;  
   });
</script>
<?php include('footer.php'); ?>