<?php
	require('connect.php'); 
    $id = $conn->real_escape_string($_POST['id']);
 
 	$sql = "select * from rrpl_database.mk_venf where id='$id'";
 	$res = $conn->query($sql);
 	$row = $res->fetch_assoc();
 
?>
 

<form method="post" action="" id="modalsave" role="form" autocomplete="off">
<div class="modal-body">
 <p style="color: #444;"> REJECT VOUCHER <button onclick="closemodal()" type="button" class="close" data-dismiss="modal" style="font-size: 30px;"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
		</p>
<div class="row">
	<?php echo "
		<input type='hidden' name='dbid' value='".$row["id"]."'> 

      	<div class='form-group col-md-6'>
				<label> Voucher No </label>
				<input type='text' class='form-control' name='' value='".$row["vno"]."' readonly /> 
		</div>
      	<div class='form-group col-md-6'>
				<label> Amount </label>
				<input type='text' class='form-control' name='' value='".$row["amt"]."' readonly /> 
		</div>


      	<div class='form-group col-md-12'>
				<label> Remark </label>
				<select class='form-control' name='remark' onChange='cng(this);' required>
					<option value=''> -- please select -- </option> 
					";

	$sql1 = "select * from rrpl_database.lr_remark";
 	$res1 = $conn->query($sql1);
 	while($row1 = $res1->fetch_assoc()){
 		echo "<option value='".$row1["title"]."'> ".$row1["title"]." </option> ";
 	}

					echo "
					<option value='OTHER'> OTHER </option> 
				</select>
		</div>


		<div class='form-group col-md-12'>
				<label> OTHER REMARK </label>
				<input type='text' class='form-control' name='other' id='other' oninput=\"this.value=this.value.replace(/[^a-z A-Z,0-9.()-]/,'')\" readonly/> 
		</div>
 

      	<div class='form-group col-md-12' style='text-align:right;'> 
		<label style='visibility: hidden;'> REJECT </label>
		<button id='donebtn' class='btn btn-danger btn-icon-split'>
		<span class='icon text-white-50'>
		<i class='fa fa-check' style='color:#fff;'></i>
		</span>
		<span class='text' style='width: 100%;'> REJECT </span>
		</button> 
		</div>
	";  

?>
</div>
</div>
</form>  

<script type="text/javascript">
function cng(sel) { 
     
	if(sel.value == "OTHER")
	{       
        $("#other").prop('required',true);
        $("#other").prop('readonly',false);  
    } else {
        $("#other").prop('required',false);
        $("#other").prop('readonly',true);  
    }
}
</script>