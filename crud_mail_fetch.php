<?php

  require('connect.php');
   
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
      $statement = $connection->prepare("SELECT * FROM rrpl_database.user where role='2'");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['ID']); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['username']); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['email']); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['incharge_name']); 
  $data[] = $sub_array;
} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>