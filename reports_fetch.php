<?php

// Array ( [seltype] => branchi [daterange] => 01/01/2020 - 01/22/2020 [fromdate] => 2020-01-01 [todate] => 2020-01-22 [branch] => ABUROAD [memoid] => )
 
	require('connect.php');
 	error_reporting(0);
    $seltype = $conn -> real_escape_string($_POST['seltype']);
    $daterange = $conn -> real_escape_string($_POST['daterange']);
    @$branch = $conn -> real_escape_string($_POST['branch']);
    $lrno = $conn -> real_escape_string($_POST['lrno']); 
    $bno = $conn -> real_escape_string($_POST['bno']); 
    // $branch = $branchuser;

function before ($thiss, $inthat)
{
    return substr($inthat, 0, strpos($inthat, $thiss));
}

function after ($thiss, $inthat)
{
    if (!is_bool(strpos($inthat, $thiss)))
        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
}
 
$fromdate = before('-', $daterange); 
$fromdate = strtotime($fromdate); 
$todate = after('-', $daterange); 
$todate = strtotime($todate);


    if($seltype=="brancho"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> Type </th> 
				<th style=""> FM/Bilty  </th> 
				<th style=""> Truck No </th> 
				<th style=""> LR No </th> 
		        <th style=""> LR Date </th>
		        <th style=""> POD Date </th> 
		        <th style=""> Extension </th> 
		        <th style=""> Upload </th> 
		        <th style=""> Pending </th> 
		        <th style=""> Consignor </th> 
		        <th style=""> From Station  </th> 
		        <th style=""> To Station  </th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_lr_outward.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

    } else if($seltype=="branchi"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> Type </th> 
				<th style=""> FM/Bilty  </th> 
				<th style=""> Truck No  </th> 
				<th style=""> LR No  </th> 
				<th style=""> LR Date </th> 
		        <th style=""> POD Date </th>
		        <th style=""> Dispatch Date </th>
		        <th style=""> Upload </th> 
		        <th style=""> Pending </th> 
				<th style=""> Consignor </th>  
		        <th style=""> From Station </th>  
		        <th style=""> To Station </th>  
		        <th style=""> Intermemo </th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_lr_inward.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" }, 
		{ mData: "11" } ,
		{ mData: "12" } ,
		{ mData: "13" } 
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
    }  else if($seltype=="memono"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th > MemoNo </th>
				<th > Memo <br> Date </th>
				<th > From <br> Station </th>
				<th > To <br> Station </th>
				<th > Dispatch <br>  Date </th>
				<th > Dispatch <br>  By </th>
				<th > Dispatch <br> Via </th>
				<th > Dispatch <br> Details </th>
				<th > Collect <br> Date </th>
				<th > Pending <br> Collect</th>
				<th > # </th>
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_memo.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" }, 
		{ mData: "9" }, 
		{ mData: "10" }, 
		{ mData: "11" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

    }  else if($seltype=="lrno"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> FM/Bilty </th> 
				<th style=""> Truck No </th> 
				<th style=""> LR No </th> 
		        <th style=""> LR Date </th>
		        <th style=""> LR Branch </th>
		        <th style=""> LR By </th>
		        <th style=""> POD Date </th> 
		        <th style=""> POD Branch </th>  
		        <th style=""> POD By </th> 
				<th style=""> Upload </th>  
		        <th style=""> Billing Party </th>  
		        <th style=""> Billing Branch </th>  
		        <th style=""> Dispatch Date </th>  
		        <th style=""> Dispatch Branch </th>  
		        <th style=""> Dispatch by </th>  
		        <th style=""> Intermemo </th> 
		        <th style=""> Collect Date </th>  
		        <th style=""> Collect Branch </th> 
		        <th style=""> Collect by</th>  
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_lrwise.php?p='.$lrno.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		// "aoColumns": [
		// { mData: "0" },
		// { mData: "1" },
		// { mData: "2" },
		// { mData: "3" },
		// { mData: "4" },
		// { mData: "5" },
		// { mData: "6" },
		// { mData: "7" },
		// { mData: "8" },
		// { mData: "9" },
		// { mData: "10" },
		// { mData: "11" },
		// { mData: "12" }
		// ],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
    }   else  if($seltype=="self"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> Type </th> 
				<th style=""> FM/Bilty  </th> 
				<th style=""> Truck No  </th> 
				<th style=""> LR No </th> 
		        <th style=""> LR Date </th>
		        <th style=""> POD Date </th> 
		        <th style=""> Consignor </th> 
		        <th style=""> Upload </th> 
		        <th style=""> POD  </th> 
		        <th style=""> Dispatch </th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_self.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

    } else if($seltype=="bno"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
			<th style=""> Sno </th> 
				<th style=""> FM/Bilty </th> 
				<th style=""> Truck No </th> 
				<th style=""> LR No </th> 
		        <th style=""> LR Date </th>
		        <th style=""> LR Branch </th>
		        <th style=""> LR By </th>
		        <th style=""> POD Date </th> 
		        <th style=""> POD Branch </th>  
		        <th style=""> POD By </th> 
				<th style=""> Upload </th>  
		        <th style=""> Billing Party </th>  
		        <th style=""> Billing Branch </th>  
		        <th style=""> Dispatch Date </th>  
		        <th style=""> Dispatch Branch </th>  
		        <th style=""> Dispatch by </th>  
		        <th style=""> Intermemo </th> 
		        <th style=""> Collect Date </th>  
		        <th style=""> Collect Branch </th> 
		        <th style=""> Collect by</th>  
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_bwise.php?p='.$bno.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		// "aoColumns": [
		// { mData: "0" },
		// { mData: "1" },
		// { mData: "2" },
		// { mData: "3" },
		// { mData: "4" },
		// { mData: "5" },
		// { mData: "6" },
		// { mData: "7" },
		// { mData: "8" },
		// { mData: "9" },
		// { mData: "10" },
		// { mData: "11" },
		// { mData: "12" }
		// ],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
    } else if($seltype=="trip"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> Trip No </th> 
				<th style=""> Truck No </th> 
				<th style=""> Average </th> 
		        <th style=""> OPENING BRANCH </th>
		        <th style=""> OPENING Date </th> 
		        <th style=""> CLOSING BRANCH </th> 
		        <th style=""> CLOSING DATE </th> 
		        <th style=""> EXTENSION </th> 
		        <th style=""> PENDING </th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_lr_trip.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		// "aoColumns": [
		// { mData: "0" },
		// { mData: "1" },
		// { mData: "2" },
		// { mData: "3" },
		// { mData: "4" },
		// { mData: "5" },
		// { mData: "6" },
		// { mData: "7" },
		// { mData: "8" },
		// { mData: "9" },
		// { mData: "10" },
		// { mData: "11" },
		// { mData: "12" }
		// ],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

    }  else if($seltype=="dno"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th > MemoNo </th>
				<th > Memo <br> Date </th>
				<th > From <br> Station </th>
				<th > To <br> Station </th>
				<th > Dispatch <br>  Date </th>
				<th > Dispatch <br>  By </th>
				<th > Dispatch <br> Via </th>
				<th > Dispatch <br> Details </th>
				<th > Collect <br> Date </th>
				<th > Pending <br> Collect</th>
				<th > # </th>
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_memo_doc.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		// "aoColumns": [
		// { mData: "0" },
		// { mData: "1" },
		// { mData: "2" },
		// { mData: "3" },
		// { mData: "4" },
		// { mData: "5" },
		// { mData: "6" },
		// { mData: "7" },
		// { mData: "8" }, 
		// { mData: "9" }, 
		// { mData: "10" }, 
		// { mData: "11" }
		// ],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

    } else if($seltype=="sumry"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th > Branch </th>
				<th > Create  </th>
				<th > Receive </th>
				<th > Nullify </th>
				<th > Self </th>
				<th > Dispatch <br> Pending </th>
				<th > Dispatch <br> Complete</th>
				<th > Destination <br> Pending </th>
				<th > Collect <br> Pending </th>
				<th > Collect <br> Complete </th>
				<th > Intermemo </th>
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_sum_data.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": false,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"language": {
"loadingRecords": "&nbsp;",
"processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
},
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		// "aoColumns": [
		// { mData: "0" },
		// { mData: "1" },
		// { mData: "2" },
		// { mData: "3" },
		// { mData: "4" },
		// { mData: "5" },
		// { mData: "6" },
		// { mData: "7" },
		// { mData: "8" }, 
		// { mData: "9" }, 
		// { mData: "10" }, 
		// { mData: "11" }
		// ],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

    }