<?php
	require('connect.php'); 
	$id = $conn->real_escape_string($_REQUEST['id']);
 	
 	$sql = $conn->query("select p.dispatchdate, p.collectdate, r.veh_type,r.frno,r.lrno,r.pod_date,r.branch,r.pod_copy from rcv_pod r left join podtrack p on p.lrid=r.id where p.memono='$id' and p.lrtype!='TRIP'
 		union all
 		select p.dispatchdate, p.collectdate, 'TRIP' as veh_type, trip_no as frno, trip_no as lrno, closing_date as pod_date, closing_branch as branch, 'NA' as pod_copy from dairy.opening_closing o left join rrpl_database.podtrack p on p.lrid = o.id where p.memono='$id' and p.lrtype='TRIP';

 		");
?>
 
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
} 
.modal-dialog{
	max-width: 700px;
}
 table {width: 100% !important;} .table>tbody>tr>td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis; text-align: center;  }
}
</style> 


<div class="modal-body" style="margin: 10px; padding-bottom: 0px;"> 
 			<div class="row">
<div class="col-md-12" >
	                   <p style="color: #444;"> Intermemo No: <?= $id; ?> <button type="button" class="close" data-dismiss="modal"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
		</p> 

	<center>
		<div class="table-responsive" style="max-height: 500px;">
	<table class="table table-bordered table-hover">
		 <thead class="thead-light"> 
			 <th>Sno.</th>
			 <th>LR_Type </th>
			 <th>FM/BILTY</th>
			 <th>LRNo</th>
			 <th>POD_Date</th>
			 <th>POD_Branch</th>
			 <th style="">Collect_Date</th>
			 <th style="">POD_Copy</th>
		</thead>
		<?php
			$sno = 0;
			while($row=$sql->fetch_assoc()){
			$sno = $sno+1;
			$pod_files1 = array(); 
			$copy_no = 0;
			foreach(explode(",",$row['pod_copy']) as $pod_copies)
			{
			    $copy_no++;
			    if (strpos($pod_copies, 'pdf') !== false) {
			      $file = 'PDF';
			    } else {
			      $file = 'IMAGE';
			    }

				if($row['veh_type']=="MARKET"){
				$pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
				} else {
				$pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
				}
			}

			echo "<tr>";
			echo "<td> ".$sno." </td>";
			echo "<td> ".$row['veh_type']."</td>";
			echo "<td> ".$row['frno']." </td>";
			echo "<td> ".$row['lrno']." </td>";
			echo "<td> ".date('d/m/Y', strtotime($row['pod_date']))." </td>";
			echo "<td> ".$row['branch']." </td>";
			
			if($row['collectdate']!=NULL || $row['collectdate']!=''){
				echo "<td> <center>".date('d/m/Y', strtotime($row['collectdate']))."</center> </td>";
			} else {
				echo "<td> <center> NA </center> </td>";
			}

			if($row['pod_copy']!='NA' || $row['pod_copy']!=''){
				echo "<td> ".implode(", ",$pod_files1)." </td>";
			} else {
				echo "<td> NA </td>";
			}
			echo "</tr>";
			
			}
	 	?>
<!-- 	</table>
	<table class="table table-bordered table-hover">
		 <tr> 
			 <td>Sno.</td>
			 <td>Bilty No</td>
			 <td>LR No</td>
			 <td>Bilty Amount</td>
			 <td>Bilty Date</td>
			 <td>POD Date</td>
			 <td>POD Branch</td>
		</tr> -->
		<?php

 		// 	$sql = $conn->query("SELECT * FROM `opening_closing` where memono='$id'");
			// // $sno = 0;
			// while($row=$sql->fetch_assoc()){
			// $sno = $sno+1;
			// echo "<tr>";
			// echo "<td> ".$sno." </td>";
			// echo "<td> ".$row['bilty_no']." </td>";
			// echo "<td> ".$row['plr']." </td>";
			// echo "<td> ".$row['tamt']." </td>";
			// echo "<td> ".$row['mdate']." </td>";
			// echo "<td> ".$row['date']." </td>";
			// echo "<td> ".$row['branch']." </td>";
			// echo "</tr>";
			
			// }
	 	?>
	</table>
	</div>
	</center>
</div> 
				</div>   
			</div>

<?php
// closeConnection($conn);
?> 