<?php

require('connect.php');
 
    $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_api.';', $DATABASE_USER, $DATABASE_PASS );
    $statement = $connection->prepare("SELECT rcv_pod.remark, rcv_pod.veh_type, freight_form_lr.crossing,rcv_pod.id,rcv_pod.ho_pod_check,freight_form_lr.fstation, freight_form_lr.tstation, lr_sample.dest_zone, rcv_pod.frno, rcv_pod.lrno, rcv_pod.branch as pod_rcvr,fm_date,fm_amount,fm_adv,fm_bal,pod_branch as fm_branch,pod_copy,pod_date, del_date,bal_date,late_charge,billing_ofc,billing_time, freight_form_lr.truck_no, freight_form_lr.consignor, freight_form_lr.consignee, freight_form_lr.wt12, freight_form_lr.weight, emps.name FROM rrpl_database.rcv_pod as rcv_pod 
        left join rrpl_database.emp_attendance as emps on emps.code = rcv_pod.branch_user

        LEFT JOIN rrpl_database.lr_sample as lr_sample on lr_sample.lrno=rcv_pod.lrno 
        left join rrpl_database.freight_form_lr on rcv_pod.frno = freight_form_lr.frno and rcv_pod.lrno=freight_form_lr.lrno
        WHERE ho_pod_check='-1' and consignor_id!='56' and remark is not null and rcv_pod.frno not like '___M%'
        group by rcv_pod.id
        ORDER BY rcv_pod.id ASC ");
    $statement->execute();
    $result = $statement->fetchAll();
    $count = $statement->rowCount();
    $data = array();

foreach($result as $row)
{ 
    $sub_array = array(); 
 
    $a = $b = "";
    if ($row["ho_pod_check"] == "1" ){
    $a = 'selected' ;
    }
    else if ($row["ho_pod_check"] == "-1" ){
    $b = 'selected' ;
    } 

 $sub_array[] = "<button onclick='showme(".$row['id'].")' class='btn btn-sm btn-warning' > <i class=\"fa fa-edit\"></i> <b>Upload</b> </button>";


if ($row["ho_pod_check"] == "1" ){
$sub_array[] = "Approved";    
}
else if ($row["ho_pod_check"] == "-1" ){
$sub_array[] = "Rejected";    
} else {
$sub_array[] = "Pending";    
}


    $sub_array[] = $row["frno"]; 
    $sub_array[] = $row["lrno"]; 
    $sub_array[] = $row["truck_no"]; 
    $sub_array[] = round($row["wt12"],2); 
    $sub_array[] = round($row["weight"],2); 
    $sub_array[] = $row["fstation"]; 
    $sub_array[] = $row['tstation'];
    $sub_array[] = $row['dest_zone'];
    $sub_array[] = $row["pod_rcvr"]; 

$pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['pod_copy']) as $pod_copies)
  {
    $copy_no++;
          
          if (strpos($pod_copies, 'pdf') !== false) {
          $file = 'PDF';
          } else {
          $file = 'IMAGE';
          }

      if($row['veh_type']=="MARKET"){
        $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
      } else {
        $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
      }
   }

if($row['pod_copy']!=''){
$sub_array[] = implode(", ",$pod_files1);
} else {
$sub_array[] = "Not Found";
}


    // $sub_array[] = "";//$row["pod_copy"]; 
    $sub_array[] = $row["pod_date"]; 
    $sub_array[] = $row["consignor"]; 
    $sub_array[] = $row["consignee"]; 
    $sub_array[] = $row["fm_branch"]; 
    $sub_array[] = $row["fm_date"]; 
    $sub_array[] = $row["fm_amount"]; 
    $sub_array[] = $row["fm_adv"]; 
    $sub_array[] = $row["fm_bal"]; 
    $sub_array[] = $row["crossing"]; 
    $sub_array[] = $row["del_date"]; 
    $sub_array[] = $row["bal_date"]; 
    $sub_array[] = $row["late_charge"]; 
    $sub_array[] = $row["billing_ofc"]; 
    $sub_array[] = $row["billing_time"]; 
    $sub_array[] = $row["remark"]; 
 
    $data[] = $sub_array;

} 

    $results = array(
      "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 